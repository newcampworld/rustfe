use std::mem::*;

fn main() {

    print!("{} {} {} {} {} {} {} {} {} {} {} {} {} {}\n\n",
    size_of::<i8>(),
    size_of::<u8>(),
    size_of::<i16>(),
    size_of::<u16>(),
    size_of::<i32>(),
    size_of::<u32>(),
    size_of::<i64>(),
    size_of::<u64>(),
    size_of::<i128>(),
    size_of::<u128>(),   // 16 bytes
    size_of::<f32>(),    // 4 bytes
    size_of::<f64>(),    // 8 bytes
    size_of::<bool>(),   // 1 byte
    size_of::<char>());  // 4 bytes

    // Print the virtual memory address of each element in the array
    let arr: [i16; 5] = [1, 2, 3, 4, 5];
    for elem in arr.iter() {
        let address = elem as *const _ as usize;
        println!("Memory address of array index{:?}: {:p}", elem, address as *const ());
        
        /*
        arr.iter() is used to create an iterator over the elements of the array.
        For each element elem, we convert it to a raw pointer using as *const _.
        This gives us a raw pointer to the element.
        We then convert the raw pointer to a usize to get the memory address.
        Finally, we print the memory address using {:p} format specifier.
        */
    }

    // Aa simpler way to print the address of any object, in hexadecimal notation:
    let t1 = true; // each boolian occupies one byte in memory
    let t2 = false;
    let t3 = true;
    let t4 = false;
    let t5 = "A";
    let t6 = "B";
    let t7 = "س";
    let t8 = "D";

    /*
    Note 1:
    If t5:0x7fff4e014a58 and t6:0x7fff4e014a68, then (68-58) 16 hex is changed, which are contiguous addresses
    The difference between t5 to t6 starts 59, 5A, 5B, 5C, 5D, 5E, 5F, 
    60, 61, 62, 63, 64, 65, 66, 67, to 68 which is one cycle change
    change from 58 to 59 requires one hex (adding 0x1) but from 58 to 68 requires two hex (adding 0x10)
    so we can say t5 occupies 2 bytes. Because, Rust uses UTF-8 encoding for string literals by default. 
    UTF-8 is a widely used encoding format that can represent all Unicode characters efficiently, 
    using 1 to 4 bytes per character depending on the code point.
    ASCII characters are indeed represented using 7 bits, but in computer systems, 
    they are often stored in 8-bit bytes (1 byte per character), and their representation can 
    be shown in hexadecimal (2 hexadecimal digits per byte). This allows for easy representation 
    and manipulation within digital systems. 

    Note 2:
    When allocating single bytes, the Rust compiler usually lays them out sequentially
    and contiguously, but when allocating larger objects, their position in memory is not
    easily predictable.
    Almost all modern processors require that elementary data have particular memory
    locations, so Rust places its objects so that they are easily accessible by the processors
    The typical alignment rule is this: “Every object of a primitive type must have an
    address that is a multiple of its own size.”
    So, while the objects occupying just one byte can be placed anywhere, the objects
    occupying two bytes can be placed only at even addresses; the objects occupying
    four bytes can be placed only at addresses that are divisible by four; and the objects
    occupying eight bytes can be places only at addresses that are a multiple of eight.
    In addition, larger objects often have addresses that are a multiple of sixteen.
    Therefore, such alignment requirements can create unused spaces, the so-called
    padding.
    */

    print!("\nt1: {:p}, t2: {:p}, t3: {:p}, t4: {:p}, \nt5: {:p}, t6: {:p}, t7: {:p}, t8: {:p}\n", 
    &t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8);

}
