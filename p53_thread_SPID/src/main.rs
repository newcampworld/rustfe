use std::thread;
use std::time::Duration;

fn main() {
    println!("Main thread starting...");

    let handles: Vec<_> = (1..=3)
        .map(|i| {
            thread::spawn(move || {
                println!("Thread {} sleeping...", i);
                thread::sleep(Duration::from_secs(44));
                println!("Thread {} woke up!", i);
            })
        })
        .collect();

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Main thread finished.");
}

/*
On Linux (and other UNIX-like systems), threads are implemented
as lightweight processes, meaning each thread gets its own PID

When a Rust program starts, it always runs in the main thread. 
This main thread gets its own PID.

If you spawn additional threads using std::thread::spawn, each 
thread typically gets its own PID. These threads will appear as 
individual entries in tools like ps or htop.

::

Steps to observe:

    1 - cargo run
    2 - ps a | grep main_p53 // to see the PID of the main thread
    3 - ps -T -p <PID> // on another teminal, we can see that SPID represents the thread IDs (one for the main thread and others for spawned threads).


*/