/*

This code combines DSCP marking with ML-based traffic classification and demonstrates Rust's capability 
to handle both network programming and machine learning effectively. Thus, is your salary 500K a year!

In this example, a synthetic dataset of network traffic features—such as packet latency, size, and protocol
type—is created and labeled as “low-priority” or “high-priority.” A logistic regression model from the 
linfa library is then trained on this dataset to learn the relationship between these features and the
assigned labels. Once trained, this model can make real-time predictions on new network packets, 
deciding whether a given packet is likely to be high-priority (e.g., real-time voice/video) or 
low-priority (e.g., bulk transfers). Based on this classification, the code modifies the DSCP bits
in the IP header (for both IPv4 and IPv6) so network devices can apply the appropriate QoS treatment.

The training itself happens on the synthetic labeled data, which is a set of known examples representing
various traffic conditions. When new or “unseen” packets arrive (in this case, simulated but could be 
real in production), their features are fed into the model to produce a priority classification. 
The predictive model is an instance (a struct) of LogisticRegression—after being trained, it’s used 
like a function (by calling .predict(...)) to output predictions. This allows the Rust program to 
dynamically assign DSCP values and thus manage QoS in an intelligent, ML-driven way.

*/
use linfa::prelude::*;
use linfa_logistic::LogisticRegression;
use ndarray::array;
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::{MutablePacket, Packet};
use pnet::transport::{transport_channel, TransportChannelType, TransportProtocol};

/// This struct simulates traffic packets we might capture.
#[derive(Debug, Clone)]
struct TrafficPacket {
    /// Raw packet data (e.g., IP header + transport header + payload)
    data: Vec<u8>,
    /// Destination IP address (could be IPv4 or IPv6)
    destination: std::net::IpAddr,
    /// Synthetic feature: measured latency in milliseconds
    latency: f64,
    /// Synthetic feature: packet size in bytes
    packet_size: f64,
    /// Synthetic feature: protocol type (TCP=1, UDP=0 in this example)
    protocol_type: f64,
}

fn main() {
    // 1. CREATE OR LOAD TRAFFIC DATASET FOR TRAINING
    // -------------------------------------------------
    // In a real scenario, you might collect these metrics from live traffic
    // and label them as high or low priority. Here, we create a synthetic dataset.
    let training_features = array![
        // latency, packet_size, protocol_type
        [10.0,  400.0,  0.0],  // Label: 0 (Low)  - e.g., small UDP
        [200.0, 1500.0, 1.0],  // Label: 0 (Low)  - large TCP
        [15.0,  600.0,  0.0],  // Label: 1 (High) - moderate-latency UDP
        [5.0,   1200.0, 1.0],  // Label: 1 (High) - low-latency TCP
        [60.0,  800.0,  0.0],  // Label: 0 (Low) 
        [8.0,   1400.0, 1.0],  // Label: 1 (High)
        [30.0,  200.0,  0.0],  // Label: 1 (High)
        [100.0, 1300.0, 1.0]   // Label: 0 (Low)
    ];

    // Labels: 0 => Low Priority, 1 => High Priority
    let training_labels = array![0, 0, 1, 1, 0, 1, 1, 0];

    // Create the Dataset
    let training_dataset = Dataset::new(training_features, training_labels);

    // 2. TRAIN THE LOGISTIC REGRESSION MODEL
    // -------------------------------------------------
    // We use logistic regression to classify each packet as High Priority (1) or Low Priority (0).
    let model = LogisticRegression::default()
        .fit(&training_dataset)
        .expect("Failed to train the logistic regression model");

    // 3. SETUP A TRANSPORT CHANNEL FOR SENDING PACKETS
    // -------------------------------------------------
    // We'll open a Layer 3 (raw) channel for IPv4. For IPv6, we can do something similar
    // with `TransportChannelType::Layer3(IpNextHeaderProtocols::Ipv6)`, though pnet usage
    // for IPv6 can differ slightly in practice.
    let protocol = TransportChannelType::Layer3(IpNextHeaderProtocols::Ip);
    let (mut tx, _rx) = transport_channel(4096, protocol)
        .expect("Failed to create transport channel");

    // 4. SIMULATE OR CAPTURE TRAFFIC, THEN CLASSIFY & MARK DSCP
    // ---------------------------------------------------------
    // Here we simulate an incoming batch of traffic packets.
    // In a real-world scenario, you might read from `_rx` or another channel.
    let packets = simulate_incoming_traffic();

    for pkt in packets {
        // EXTRACT FEATURES FOR CLASSIFICATION
        let sample = array![[pkt.latency, pkt.packet_size, pkt.protocol_type]];
        let predicted_label = model.predict(&sample)[0]; // 0 or 1

        // DECIDE DSCP BASED ON THE PREDICTION
        // Example DSCP values:
        //   High-priority = EF (46) or AF4x range
        //   Low-priority  = CS1 (8) or default (0)
        let dscp_value = if predicted_label == 1 {
            46 // EF
        } else {
            8  // CS1
        };

        // MARK THE DSCP BITS
        let mut packet_data = pkt.data.clone();

        // Check if IPv4 or IPv6, then set DSCP bits accordingly:
        match pkt.destination {
            // IPv4
            std::net::IpAddr::V4(_) => {
                set_dscp_ipv4(&mut packet_data, dscp_value);
            }
            // IPv6
            std::net::IpAddr::V6(_) => {
                set_dscp_ipv6(&mut packet_data, dscp_value);
            }
        }

        // FINALLY, SEND THE PACKET WITH UPDATED DSCP
        // Destination is determined by the IP in the packet or `pkt.destination`
        if let Err(e) = tx.send_to(&packet_data, pkt.destination) {
            eprintln!("Failed to send packet: {:?}", e);
        } else {
            println!(
                "Packet sent to {:?} with DSCP={}. Classified as {} priority.",
                pkt.destination,
                dscp_value,
                if predicted_label == 1 { "HIGH" } else { "LOW" }
            );
        }
    }
}

/// Simulates a few incoming traffic packets that we want to classify and mark.
/// In a real-world scenario, you might capture them via pnet or another library.
fn simulate_incoming_traffic() -> Vec<TrafficPacket> {
    vec![
        TrafficPacket {
            data: create_dummy_ipv4_packet(400),
            destination: "192.168.1.10".parse().unwrap(),
            latency: 25.0,
            packet_size: 400.0,
            protocol_type: 0.0, // UDP-like
        },
        TrafficPacket {
            data: create_dummy_ipv4_packet(1400),
            destination: "192.168.1.11".parse().unwrap(),
            latency: 10.0,
            packet_size: 1400.0,
            protocol_type: 1.0, // TCP-like
        },
        TrafficPacket {
            data: create_dummy_ipv6_packet(600),
            destination: "2001:db8::1".parse().unwrap(),
            latency: 50.0,
            packet_size: 600.0,
            protocol_type: 0.0, // UDP-like
        },
    ]
}

/// Creates a dummy IPv4 packet with a certain size.
/// Here, we only fill minimal IP header bytes for demonstration.
/// In a real scenario, you'd capture or build valid packets.
fn create_dummy_ipv4_packet(payload_len: usize) -> Vec<u8> {
    // Minimal IPv4 header is 20 bytes
    let total_len = 20 + payload_len;
    let mut packet = vec![0u8; total_len];
    
    // Version (4 bits) + IHL (4 bits) -> 0x45 => IPv4, 5 * 4 bytes = 20
    packet[0] = 0x45;

    // Total Length in bytes (big-endian)
    packet[2] = ((total_len >> 8) & 0xFF) as u8;
    packet[3] = (total_len & 0xFF) as u8;

    // We'll leave other header fields zeroed for demonstration
    packet
}

/// Creates a dummy IPv6 packet with a certain payload size.
/// Minimal IPv6 header is 40 bytes.
fn create_dummy_ipv6_packet(payload_len: usize) -> Vec<u8> {
    // Minimal IPv6 header is 40 bytes
    let total_len = 40 + payload_len;
    let mut packet = vec![0u8; total_len];

    // First byte: version (4 bits) is 6, so 0x60 = 01100000
    packet[0] = 0x60;

    // Payload length at bytes [4..6] (16-bit big-endian)
    let payload_len16 = payload_len as u16;
    packet[4] = ((payload_len16 >> 8) & 0xFF) as u8;
    packet[5] = (payload_len16 & 0xFF) as u8;

    // Next Header, Hop Limit, etc., left at 0 for demonstration
    packet
}

/// Sets DSCP bits in an IPv4 packet buffer.
/// Byte 1 of the IPv4 header contains DSCP (upper 6 bits) + ECN (lower 2 bits).
fn set_dscp_ipv4(packet: &mut [u8], dscp: u8) {
    if packet.len() < 20 {
        eprintln!("Packet too short to be valid IPv4");
        return;
    }
    
    // The 'Type of Service' / 'DiffServ' byte is at index 1 in IPv4.
    // DSCP = upper 6 bits, ECN = lower 2 bits => we shift DSCP by 2 bits.
    let ecn_mask = packet[1] & 0b00000011; // preserve existing ECN bits
    packet[1] = (dscp << 2) | ecn_mask;
}

/// Sets DSCP bits in an IPv6 packet buffer.
/// The Traffic Class field is split across the first and second byte of the IPv6 header:
///   - Bits 0-3 of byte 0 (after version) + bits 0-3 of byte 1 => Traffic Class
/// For simplicity, we’ll ignore Flow Label for demonstration and just set DSCP in bits.
fn set_dscp_ipv6(packet: &mut [u8], dscp: u8) {
    if packet.len() < 40 {
        eprintln!("Packet too short to be valid IPv6");
        return;
    }

    // IPv6 header:
    // Byte 0: Version (4 bits) + Traffic Class high nibble (4 bits)
    // Byte 1: Traffic Class low nibble (4 bits) + Flow Label high nibble (4 bits)
    //
    // We want to set the full 8 bits for Traffic Class = DSCP(6 bits) + ECN(2 bits).
    // DSCP occupies the upper 6 bits, ECN are the lower 2 bits.

    // Let's preserve the version (bits 4..8) and flow label bits (bits 12..20).
    // We only manipulate bits 4..11 (the 8 bits of Traffic Class).

    // Current 'Traffic Class' is spread across:
    //   - high nibble of packet[0] (bits 0..3 of Traffic Class)
    //   - low nibble of packet[1] (bits 0..3 of Traffic Class)
    //
    // We can unify them into a single byte, set DSCP, then write back.

    // Extract the version nibble from packet[0]
    let version_and_tc_high = packet[0]; // e.g., 0x60
    let version = version_and_tc_high & 0xF0; // upper 4 bits
    // Lower 4 bits of packet[0] = high nibble of Traffic Class
    let tc_high_nibble = version_and_tc_high & 0x0F;

    // Lower nibble of the Traffic Class is bits 4..7 of packet[1]
    let tc_low_nibble = (packet[1] & 0xF0) >> 4;
    // The top nibble of packet[1] is the high nibble of the Flow Label (not to be overwritten entirely)

    // Reconstruct the current traffic class
    let current_traffic_class = (tc_high_nibble << 4) | tc_low_nibble; // 8 bits

    // The lower 2 bits of the traffic class are the ECN bits, which we might want to preserve
    let ecn_mask = current_traffic_class & 0x03; // keep only the lowest 2 bits

    // DSCP is the upper 6 bits
    let new_tc = (dscp << 2) | ecn_mask; // 6 bits DSCP + 2 bits ECN

    // Now we split `new_tc` back into high nibble and low nibble
    let new_tc_high_nibble = (new_tc & 0xF0) >> 4;
    let new_tc_low_nibble = new_tc & 0x0F;

    // Write back to packet[0] (version + new TC high nibble)
    packet[0] = version | new_tc_high_nibble;

    // Write back to packet[1] (new TC low nibble + old flow label high nibble)
    let flow_label_high_nibble = packet[1] & 0x0F;
    packet[1] = (new_tc_low_nibble << 4) | flow_label_high_nibble;
}
