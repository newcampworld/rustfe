enum IPAddr {
    V4(u8, u8, u8, u8), //Tuple-like variant
    V6(String),
}

fn main() {
    let localhost_v4 = IPAddr::V4(127, 0, 0, 1);
    let localhost_v6 = IPAddr::V6(String::from("::1"));

    match localhost_v4 {
        IPAddr::V4(a, b, c, d) => println!("IPv4 address: {}.{}.{}.{}", a, b, c, d),
        IPAddr::V6(addr) => println!("IPv6 address: {}", addr),
    }

    /*
    In every pattern there must be as many arguments as the fields defined in their
respective declaration. In addition, the types of the literals must be the same of the
respective fields.
    */

    match localhost_v6 {
        IPAddr::V4(a, b, c, d) => println!("IPv4 address: {}.{}.{}.{}", a, b, c, d),
        IPAddr::V6(addr) => println!("IPv6 address: {}", addr),
    }
}

/*
The use of trailing commas in programming languages, 
including Rust, has historical roots in various programming 
languages and coding practices.

One of the early influences can be traced back to the C language. 
In C, trailing commas were not allowed in array initializers. H
owever, in C99, a version of the C standard released in 1999, 
this restriction was lifted, allowing trailing commas in array 
initializers. This change aimed to facilitate code maintenance 
and version control systems by making it easier to add or remove 
elements from arrays without having to adjust the commas on surrounding lines.
*/