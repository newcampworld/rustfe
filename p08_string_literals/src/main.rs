// 

fn main() {

    // Regular String Literal
    let bytes: &'static [u8; 5] = b"Hello"; // we can omit &'static [u8; N] and just write let bytes = b"Hello"
    // This points to a fixed array u8 [72, 101, 108, 108, 111] in the program’s binary.
    
    // Byte String Literal
    let s: &'static str = "Hello"; // we can omit the explicit :&'static str and just write let s = "Hello";
    // This points to a static, immutable UTF-8 string in the program’s binary.
    /*
    byte string literals &'static [u8; N] and string literals &'static str are references with a 'static lifetime
    Both are embedded in the program at compile time and stay valid as long as the program runs
    */

    // Raw String Literal
    let raw_path = r"C:\Users\Adib\Documents"; // No double-escaping needed
    // When you want to include backslashes or other characters literally, without needing to escape them.

    // Raw Byte String Literal
    let raw_bytes = br"Line1\nLine2";
    // When you want a byte slice and also want raw literal handling, i.e., no interpretation of escape sequences.

}