/*
In PoS, validators are chosen to propose and verify new blocks based on the amount of 
cryptocurrency they have staked (locked) in the network, acting as a financial commitment
to honest behavior. The selection process is often weighted by the size of the stake and
may include randomness to ensure fairness. Validators earn rewards for participating,
and malicious behavior can result in penalties, such as losing part of their stake (slashing). 
This approach is more energy-efficient and scalable than PoW while maintaining decentralization and security.
*/

use rand::Rng; // For random number generation using ChaCha12 algorithm
use std::collections::HashMap;

#[derive(Debug)]
struct Validator {
    name: String,
    stake: u64, // Amount of cryptocurrency staked
    reward: u64, // Reward balance
}

fn main() {
    // Step 1: Initialize validators with stakes
    let mut validators = vec![
        Validator {
            name: String::from("Alice"),
            stake: 50,
            reward: 0,
        },
        Validator {
            name: String::from("Bob"),
            stake: 30,
            reward: 0,
        },
        Validator {
            name: String::from("Charlie"),
            stake: 20,
            reward: 0,
        },
    ];

    // Step 2: Simulate block proposer selection
    let total_stake: u64 = validators.iter().map(|v| v.stake).sum();
    let mut rng = rand::thread_rng();
    /*
    rand::thread_rng() gets a thread-local RNG (RNG instance (ThreadRng)) and it cannot be shared across threads.
    It avoids contention and ensures that RNG operations are safe and efficient in multi-threaded programs.
    If an RNG instance does not exist for the current thread, it creates one. 
    This instance is cached for future calls within the same thread.
    Each thread initializes its RNG instance initialized with a seed derived from the /dev/urandom
    The RNG instance keeps track of its internal state (stateful), which evolves with every random number generated.

    We don’t call Rng directly in the code because it’s a trait, not a struct or a function. Instead,
    the struct ThreadRng implements the Rng trait, so you access gen_range (a method of the Rng trait) 
    through an instance of ThreadRng:
        rand::thread_rng() creates an instance of ThreadRng
        The ThreadRng struct implements the Rng trait, so you can call gen_range on it.
    */

    // Number of blocks to simulate
    let blocks_to_produce = 10;

    for _ in 0..blocks_to_produce {
        let random_value = rng.gen_range(0..total_stake);
        let mut cumulative_stake = 0;
        let mut selected_validator: Option<&mut Validator> = None;

        // Select validator based on stake weight
        for validator in &mut validators {
            cumulative_stake += validator.stake;
            if random_value < cumulative_stake {
                selected_validator = Some(validator);
                break;
            }
            /*
            Weighted Probabilities:
                Alice: 50% probability → She occupies the first half of the range [0, 50).
                Bob: 30% probability → He occupies the next 30% of the range [50, 80).
                Charlie: 20% probability → He occupies the final 20% of the range [80, 100).
            */
        }

        /*
        Example:
        block = 0, cumulative_stake = 0, random_value = 65
        First Iteration (Alice):
            cumulative_stake += Alice's stake → cumulative_stake = 0 + 50 = 50. 65 < 50 → False.
        Second Iteration (Bob):
            cumulative_stake += Bob's stake → cumulative_stake = 50 + 30 = 80. 65 < 80 → True.
        Third Iteration (Charlie):
            This iteration is never reached because the loop breaks after selecting Bob.
        */

        if let Some(validator) = selected_validator {
            // Reward the selected validator
            validator.reward += 10; // Fixed reward for simplicity
            println!("Block produced by: {}", validator.name);
        }
        // Randomness ensures fairness, while weighted probabilities ensure that stake size influences selection.
    }

    // Step 3: Display final rewards and stakes
    println!("\nFinal Validator States:");
    for validator in validators {
        println!(
            "Validator: {}, Stake: {}, Rewards: {}",
            validator.name, validator.stake, validator.reward
        );
    }
}

/*
Why Weighted Probabilities Are Important in PoS

    Fairness: Validators with more stake are more likely to be selected, aligning rewards with their contribution to the network.
    Decentralization: Validators with smaller stakes still have a chance to be selected, maintaining participation.
    Security: Weighted probabilities ensure that attackers need to control a majority of the stake to dominate block production.

    The mathematical concept behind PoS is rooted in weighted random sampling using cumulative distribution functions. 
    Each validator's selection probability is proportional to their stake, ensuring fairness and alignment of incentives. 
    This model was first proposed by Sunny King and Scott Nadal for Peercoin in 2011 and has since become a 
    cornerstone of modern blockchain design.
*/
