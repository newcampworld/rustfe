fn main(){

    let mut arr = [4, 8, 1, 10, 0, 45, 12, 7];
    println!("{:?}", arr);
    arr.sort();
    println!("{:?}", arr);
    // version 4: more and more simplification. no need to use std
    arr.sort_by(|a ,b| b.cmp(a));
    println!("{:?}", arr);
    /*
    That closure was born in the following ways:
    use std::cmp::Ordering;
    // version 1 (no closure):
    fn desc(a: &i32, b: &i32) -> Ordering {
        if a < b { Ordering::Greater }
        else if a > b { Ordering::Less }
        else { Ordering::Equal }
    arr.sort_by(desc);
    }
    // version 2: let keyword instead of fn; using | instead of brace; using = and ;
    let desc = |a: &i32, b: &i32| -> Ordering {
    if a < b { Ordering::Greater }
    else if a > b { Ordering::Less }
    else { Ordering::Equal }
    };
    // version 3: more simplification
    arr.sort_by(|a, b|
    if a < b { Ordering::Greater }
    else if a > b { Ordering::Less }
    else { Ordering::Equal });
    */

    let factor = 2;
    println!("Anonymous Closure (|a| a * factor)(13): {}", (|a| a * factor)(13)); 
    let multiply = |a| a * factor;
    println!("Named Closure let multiply = |a| a * factor: {}", multiply(13)); 

    /*
    The closure (|a| a * factor) is designed to take an argument a; thus println!("{}", |a| a * factor) fails
    However, println!("{}", (|| 10 * factor)()); succeeds
    Therefore, in the expression: (|a| a * factor)(13), we are doing several things in one concise statement:
        Define the Closure Signature:  |a| declares a closure that takes a single parameter named a
        Define the Closure Body:        a * factor is the body of the closure. 
        Pass an Argument and Invoke:   (13) immediately invokes the closure with 13 as the argument, assigning a = 13
    */

    // Forms of declaring and invoking anonymous closures
    println!("Forms of invoking closures: {}\n{}\n{}\n",
        (|b| b * factor)(13),
        (|b: i32| b * factor)(13), // Rust infers parameter and return types, but you can specify them explicitly
        |b| -> i32 { b * factor }(13) // body is enclosed in braces to seperate it from return type
    );
    // Closeure can contains several statements so braces are required
    println!(
        "Closure with multimple statements: {}",
        (|v: &Vec<i32>| {
            let mut sum = 0;
            for i in 0..v.len() {
                sum += v[i];
            }
            sum
        })(&vec![11, 22, 34])
    );

    // Functions like sort_by, rev, fold, filter, or map let you pass closures to specify custom behavior
    
    let nums = vec![5, 8, 3, 9, 15, 36, 4, 12];
    let doubled: Vec<_> = nums.iter().map(|x| x * 2).collect();
    // _ in Vec<_> means "let the compiler deduce the type."
    // an iterator over &nums elements: &5, &8, &3, &9 is created
    // x represents a reference (&i32) to each element in the vector
    // map() applies the closure |x| x * 2 to each element of the iterator
    // collect () consumes the iterator and collects the results into a new vector (Vec<i32>)
    println!("Doubled {:?}", doubled);

    let sum: i32 = nums.iter().fold(0, |acc, &x| acc + x);
    // fold() Takes two arguments: An initial value (0 in this case) and 
    // a closure (|acc, &x| acc + x) that defines how to combine the elements.
    // acc + x = 0 + 5 = 5, acc + x = 5 + 8 = 13, ...
    println!("Folded: {}", sum);

    let evens: Vec<i32> = nums.iter().cloned().filter(|x| x % 2 == 0).collect();
    // .iter() works on references, so you need .cloned() or .copied() to collect into owned values
    println!("Evens: {:?}", evens);

    let reversed: Vec<i32> = nums.iter().rev().cloned().collect();
    println!("Reversed: {:?}", reversed);

}

