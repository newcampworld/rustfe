// Composition

/*
It was realized that Inheritnace is a fantastic solution. As years went by, it was realized that data inheritance created
more problems than it solved, so Rust does not support it. Let's break down some of the issues:

Tight Coupling:
    When a base class is modified, it can unintentionally break or alter the behavior of derived classes, 
    leading to unpredictable outcomes and bugs that are difficult to trace.

The Diamond Problem:
    When two classes inherit from a common base class and a third class inherits from both, 
    leading to ambiguity about which base class’s implementation to use.

Inheritance Hierarchy Complexity:
    Deep and complex inheritance hierarchies can make the code difficult to understand, maintain, and extend.
    They can lead to a rigid structure that's hard to refactor.

Violation of Encapsulation:
    Derived classes have access to the protected and public members of the base class,
    which can lead to unintended dependencies and violation of the base class's encapsulation.

In place of data inheritance, Rust uses composition as follows:

Traits for Behavior Sharing:
    Instead of inheriting behavior through a base class, Rust uses traits to define shared behavior. 
    Traits are similar to interfaces in other languages, allowing types to implement specific behavior without inheriting data or structure.

Structs for Data Composition:
    Rust encourages the use of structs to compose data. Rather than inheriting data, a struct can contain other structs, 
    allowing for flexible and modular data composition.

Explicit Delegation:
    If a struct needs to expose functionality of a contained type, Rust requires explicit delegation. 
    This approach avoids the pitfalls of implicit inheritance and provides clear, maintainable code.

*/

struct Engine {
    horsepower: u32,
}

struct Car {
    brand: String,
    engine: Engine,
}

impl Car {
    fn start(&self) {
        println!("{} car with {} horsepower engine is starting!", self.brand, self.engine.horsepower);
    }
}

fn main() {
    let engine = Engine { horsepower: 300 };
    let car = Car {
        brand: String::from("Toyota"),
        engine,
    };

    car.start();
}

/*
This approach ensures that the code is both safe (with no inheritance-related issues) and 
flexible (you can easily change how structs are composed without affecting unrelated parts of the code).
*/