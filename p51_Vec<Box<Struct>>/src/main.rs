use std::io::{self, Write};

#[derive(Debug)]
struct Message {
    id: usize,
    content: String,
}

impl Message {
    fn new(id: usize, content: &str) -> Box<Message> {
        // Create a heap-allocated message
        Box::new(Message {
            id,
            content: content.to_string(),
        })
    }
}

fn main() {
    let mut stdout = io::stdout();

    let mut message_queue: Vec<Box<Message>> = Vec::new();
    /*
    Create a vector to store heap-allocated messages
    A Vec in Rust consists of the following components:
        - Metadata (Pointer, Length, Capacity) is always stored on the stack (usually 3 usize values)
            - Pointer: A pointer to the memory location where the elements are stored. This points to heap memory when the Vec has elements.
            - Length: The number of elements currently in the Vec.
            - Capacity: The amount of memory currently allocated for the Vec on the heap.
        - Contents of the Vec. The actual data (the elements) is stored on the heap

    Vec::new() creates an empty vector on the stack. At this point:
        Pointer: null (no heap memory allocated yet).
            No memory is allocated on the heap for storing the Box<Message> pointers.
            This behavior is intentional and reflects Rust's lazy allocation strategy. Because, 
            until it’s actually needed to avoid wasting memory for unused or empty vectors.
        Length: 0.
        Capacity: 0.
    
    After message_queue.push(Message::new(1, "Hello, world!"));
        Pointer: Points to the heap memory for the array of Box<Message> pointers.
        Length: 1 (the vector now contains one element).
        Capacity: 4 (heap memory for up to 4 pointers has been allocated).
            Rust's Vec uses an exponential growth strategy for resizing its capacity: (e.g., from 4 to 8, 8 to 16, etc.).
            It can be controlled at: let mut message_queue: Vec<Box<Message>> = Vec::with_capacity(4);


    After message_queue.push(Message::new(2, "Rust is awesome!"));
        Pointer: Unchanged (still pointing to the same heap-allocated array for Box<Message> pointers).
        Length: Updated to 2.
        Capacity: Unchanged at 4.
    */

    message_queue.push(Message::new(1, "Hello, world!"));
    message_queue.push(Message::new(2, "Rust is awesome!"));
    message_queue.push(Message::new(3, "Vec<Box<T>> is powerful!"));

    writeln!(stdout, "Messages in the queue:").expect("Failed to write to stdout");
    for message in &message_queue {
        writeln!(stdout, "ID: {}, Content: {}", message.id, message.content)
            .expect("Failed to write to stdout");
    }

    if let Some(processed_message) = message_queue.pop() {
        // to check whether an enum value is a certain variant, and in that case you need to extract its fields
        writeln!(
            stdout,
            "Processing message ID: {}, Content: {}",
            processed_message.id, processed_message.content
        )
        .expect("Failed to write to stdout");
    }

    writeln!(stdout, "Remaining messages in the queue:").expect("Failed to write to stdout");
    for message in &message_queue {
        writeln!(stdout, "ID: {}, Content: {}", message.id, message.content)
            .expect("Failed to write to stdout");
    }
}
