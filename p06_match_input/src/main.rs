use std::io;

enum TrafficLight {
    Red,
    Yellow,
    Green,
}

fn main() {
    // Prompt the user for input
    println!("Enter the traffic light color (red, yellow, green):");

    // Create a new String to store the user input
    let mut input = String::new();

    // Read the user input from standard input
    io::stdin().read_line(&mut input).expect("Failed to read line");

    // Trim the input to remove any trailing whitespace
    let input = input.trim().to_lowercase();

    // Match the user input to the TrafficLight enum
    let light = match input.as_str() {
        // converts the user input to a TrafficLight enum variant
        // creates an instance of the variant of the TrafficLight enum
        "red" => TrafficLight::Red,
        "yellow" => TrafficLight::Yellow,
        "green" => TrafficLight::Green,
        _ => {
            println!("Invalid input. Please enter red, yellow, or green.");
            return;
        }
    };

    // Use match to handle different variants of TrafficLight
    match light {
        TrafficLight::Red => println!("Stop!"),
        TrafficLight::Yellow => println!("Prepare to stop."),
        TrafficLight::Green => println!("Go!"),
    }
    // The match expression is used to handle the different variants of the TrafficLight enum. 
    // For each variant, we provide a corresponding action (in this case, printing a message).
}
