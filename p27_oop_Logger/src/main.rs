/*
In Rust, memory is already released by the language,
and therefore there is no need to invoke functions similar to the free function of
C language, or to the delete operator of C++ language. But other resources are not
automatically released. Therefore destructors are most useful to release resources
like file handles, communication handles, GUI windows, graphical resources, and
synchronization primitives. If you use a library to handle such resources, that library
should already contain a proper implementation of the Drop trait for any of its types that
handle a resource.
*/


use std::fs::File;
use std::io::{self, Write};

struct Logger {
    file: File,
}

struct S(i32);

impl Drop for S {
    fn drop(&mut self) {
        println!("Dropped {}", self.0);
    }
} 

impl Logger {
    // Create a new Logger instance
    fn new(filename: &str) -> io::Result<Logger> {
        let file = File::create(filename)?;
        Ok(Logger { file })
    }

    // Log a message to the file
    fn log(&mut self, message: &str) {
        writeln!(self.file, "{}", message).expect("Failed to write to log file");
    }
}

// Implementing Drop trait to release the file handle

impl Drop for Logger {
    fn drop(&mut self) {
        /*
        Such trait, defined by the Rust language, has the peculiar
        property that its only* method, named drop, is automatically invoked exactly when the
        object is deallocated, and therefore it is a destructor

        In general, to create a destructor for a type, it is enough to implement the Drop trait for such type
        */
        println!("Closing log file.");
        // The file handle will be automatically closed here
        // as File implements the Drop trait, but you could perform other cleanup tasks if needed.
    }
}

/*
If we were to comment out the Drop trait, the lsof | grep log.txt, does not show anything because Rust automatically 
drop the Logger instance when it goes out of scope, and the file handle will be closed because File itself implements the Drop trait.
*/

fn main() {
    {
        let mut logger = Logger::new("log.txt").expect("Failed to create log file");
        logger.log("This is a log message.");
    } // The logger goes out of scope here, and the Drop trait is invoked, releasing the file handle.

    println!("Logger has been dropped, and the file is closed.");

    let _s1 = S(1);
    let _s2 = S(2);
    let _s3 = S(3);

    {
        let _s4 = S(4);
        let _s5 = S(5);
        let _s6 = S(6);
        println!("INNER");
    } // `_s4`, `_s5`, and `_s6` are dropped here.

    println!("OUTER");

} // `_s1`, `_s2`, and `_s3` are dropped here.


