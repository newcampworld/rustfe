fn main() {

    /*
    Slicing: The opration of taking a slice from an array or a vector.
    The term "slice" in Rust refers to a view into a contiguous sequence of elements within a collection, like an array, vector, or string.
    Slices are inherently references and are often referred to by their type:
        Immutable Slice: &[T]
        Mutable Slice: &mut [T]
    */
    let mut arr = [11, 22, 33, 44, 55, 66, 77, 88];
    println!("Array1: {:?}", arr);
    println!("A slice of array1: {:?}", &arr[2..4]);
    {
        // we use this scope to show that how original array changes despite having operation inside the scope
        let sl_ref = &mut arr[3..6]; // creates a mutable slice.
        println!("Slicing array1 3-6 {:?}", sl_ref);
        sl_ref[1] = 0; // the slice is borrowed mutably using &mut, so modifying the elements within the slice is permited, otherwise, operation fails
        println!("Slice changed due to its mutability {:?}", sl_ref);
    }
    /*
    A slice is always a borrowed view into a portion of the original collection (like an array, vector, or string). 
    Any changes made through a mutable slice (&mut [T]) directly modify the underlying collection 
    because the slice is not a copy—it is a reference to the original data.
    */
    println!("Array1: {:?}", arr);
    
    let sl_ref = &arr[3..6]; // Requires .iter() or &slice for iteration because they do not directly implement IntoIterator
    for value in sl_ref.iter() { // Explicitly create an iterator or use "for value in &sl_ref"
        println!("An immutable slice of array1: {}", value);
    }

    // Ranges
    let range_a = 14u32 .. 32; // defining a type for the first extreme is adequate
    let range_b = -14i32 .. 32;

    println!("Range a: {:?}, with size of {}\nRange b: {:?}, with size of {}",
        range_a, range_a.len(),
        range_b, range_b.len());

    /*
    Replacing the original code with an explicit type using std::ops::Range<usize> makes 
    the type and intention of the range clearer and more type-safe.
    The usize type is often used for indexing collections because it's guaranteed to be the 
    correct size for addressing memory on the current platform (32-bit or 64-bit). 
    Using usize for ranges, especially in indexing or slicing operations, avoids potential 
    issues related to mismatched types or integer overflows.
    */

    // Using std::ops::Range<usize> is more aligned with Rust’s idiomatic practices
    let range_c: std::ops::Range<usize> = 14..32;
    println!("Range c: {:?}", range_c);

    /*
    The following function 'min' takes the minimum of an array with the following drawbacks:
        1. the argument is sent by copy
        2. a portion of the array cannot be processed
        3. it cannot receive a vector
        4. it can receive only eight-number-array
    */
    fn min(arr: [i32; 8]) -> i32 {
        let  mut minimum = arr[0];
        for i in 1..arr.len() {
            if arr[i] < minimum { minimum = arr [i];}
        }
        minimum
    }
    println!("Minimum with no slice referencing: {}", min([94,99,164,73,82,86,91,512]));
    /*
    Several ways to pass an array to a function:
        fn sum(arr: [i32; 4]) -> i32 {
        fn sum(arr: &[i32; 4]) -> i32 {
        fn sum(arr: &[i32]) -> i32 {
        fn double(arr: &mut [i32]) {

    The last two ones are slice reference.  They allow you to pass a portion of an array or even dynamically-sized arrays. 
    This method is often preferred because it works with arrays of any size.
    To overcome aforementioned drawbacks, we use slice reference as follows:
    */
    fn min2(arr: &[i32]) -> i32 {
        let mut minimum = arr[0];
        for i in 1..arr.len() {
            if arr[i] < minimum { minimum = arr[i]; }
        }
        minimum
    }
    println!("Minimum with slice referencing: {} ", min2(&[23, 17]));
    println!("Minimum with slice referencing: {}", min2(&vec![78,33,812,14,31]));
    println!("Minimum with slice referencing: {}", min2(&vec![78,33,812,14,31,98,46,178,67,37,67][5..8]));
    
    // Open-Ended Ranges and Slicing:
    let r1: std::ops::RangeFrom<usize> = 3..;
    let r2: std::ops::RangeTo<usize> = ..4;
    let arr2 = [9, 6, 4, 8, 6, 18, 21, 2, 0, 7, 88];
    println!("Array2: {:?}", arr2);
    println!("Open-Ended Range of array2 with rangeFrom r1: {:?}", &arr2[r1]);
    println!("Open-Ended Range of array2 with rangeTo r2: {:?}", &arr2[r2]);

    // Loop with an open-ended range starting from 4
    for i in 4.. {
        if i * i > 56 {break;}
        println!("Our loop breaks at {} because i*i={} is less than 56", i, i*i);
    }


}