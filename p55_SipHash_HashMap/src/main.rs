/*
DefaultHasher is an alias for the default implementation of a hashing algorithm in Rust.
In the current implementation of Rust's standard library, the default hashing algorithm 
used by DefaultHasher is SipHash-1-3, a variant of the SipHash algorithm.
*/

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

fn main() {
    let keys: Vec<String> = vec![
        "node01".to_string(),
        "node02".to_string(),
        "node03".to_string(),
        "node04".to_string(),
        "node05".to_string(),
        "node06".to_string(),
        "node07".to_string(),
    ];
    let mut hash_values: Vec<u64> = Vec::new(); // Vector to store hash values
    
    for key in &keys {
        // Create a new SipHash hasher for each key
        let mut hasher = DefaultHasher::new();
        // Hash the key
        key.hash(&mut hasher);
        let hash_value = hasher.finish();
        
        hash_values.push(hash_value);
        println!("Hash value for key '{}' is: {}", key, hash_value);
    }
}

/*
In HashMap data structure, when a key is inserted into a HashMap, it is processed 
by SipHash that transforms it into a hash value. The hash value is then mapped to a 
specific bucket index using a modulo operation:

    bucket index = hash value % number of buckets

For the above keys, for instance, the following hash values are generated:

    Hash value for key 'node01' is: 14223867837164225906
    Hash value for key 'node02' is: 5630739670760167520
    Hash value for key 'node03' is: 14708300492622666044
    Hash value for key 'node04' is: 13970215330295729774
    Hash value for key 'node05' is: 5308130427127026028
    Hash value for key 'node06' is: 11879425172180262440
    Hash value for key 'node07' is: 7225244329611017098

Then, each key-value pair is stored in its bucket. Next time To find a key, the HashMap 
computes the hash value of the key and directly determines its bucket index, eliminating 
the need to search through the entire collection. I large data sets, without hashing, 
locating a key would typically require linear search (O(n)) or 
binary search (O(log n)) in sorted structures. However, with HashMap, time-complexity is O(1).

Buckets would be as follows: 

    Bucket 0: [ ("node3", value3) ]
    Bucket 1: [ ("node2", value2) ]
    Bucket 2: [ ("node1", value1) ]
    Bucket 3: [ ("node7", value7) ]
    Bucket 4: [ ("node4", value4) ]
    Bucket 5: [ ("node7", value5) ]
    Bucket 6: [ ("node5", value6) ]
    Bucket 7: [ ("node6", value7) ]

If two keys produce the same hash value, Rust resolves the collision using chaining 
(storing multiple elements in the same bucket). Hash collisions occur when multiple 
keys are hashed to the same bucket. If the number of buckets is small relative to the 
number of keys, the likelihood of two keys colliding increases. The hash function 
ensures that hash values are evenly distributed across buckets, minimizing the 
likelihood of collisions. With no collisions, lookups are purely O(1).

e.g: Hash of key node4 is 13970215330295729774, then it modules 32, which is bucket size, equals 14

*/
