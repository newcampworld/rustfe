extern crate openssl;

use openssl::x509::X509;
use std::fs::File;
use std::io::Read;

fn main() {
    let mut cert_file = File::open("/home/pshko/sample.cer").unwrap();
    let mut pem_data = Vec::new();
    cert_file.read_to_end(&mut pem_data).unwrap();

    let x509 = X509::from_pem(&pem_data).unwrap();

    let subject = x509.subject_name();
    let issuer = x509.issuer_name();
    let public_key = x509.public_key().unwrap();

    println!("Subject: {:?}", subject);
    println!("Issuer: {:?}", issuer);
    println!("Public Key: {:?}", public_key);
}