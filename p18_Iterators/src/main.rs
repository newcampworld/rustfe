/*
When we create an iterator over an object, we establish a systematic way to traverse 
through its elements one at a time, abstracting away the details of how the elements 
are stored or accessed. Philosophically, this process embodies the idea of deferred 
computation: rather than consuming all elements at once, the iterator provides a 
controlled, step-by-step mechanism to access them as needed.

whether the data is stored in a Vec, a HashMap, or generated dynamically, the iterator 
provides a consistent way to access elements.

Methods like .map(), .filter(), and .fold() allow you to express complex operations declaratively 
without manually handling loops, aligning with the principle of separating the “what” from the “how.”

Rust’s iterators respect ownership and borrowing rules. 
    into_iter(): Transfers ownership of each element and consumes the collection.
    iter(): Borrows each element immutably, allowing read-only access.
    iter_mut(): Borrows each element mutably, allowing modifications.

Iterators in Rust are lazy; computation happens only when needed (e.g., during .collect() or .fold()).
*/
fn main(){

    print!("-----------Printing all chars $€9: {}","\n");
    // To get a byte from a string, we need to convert the string to a slice of bytes
    let s = "$€9";
    println!("Lenght of s is {}", s.len());
    for i in 0..s.len(){
        println!("{}",  s.as_bytes()[i]);
        // cost is zero because the representation of a string buffer is that sequance of bytes
    }

    /*
    UTF-8 representation of any ASCI character is just the ASCI code of that character
    The result is 36, 226, 130, 172 (3 bytes for the Euro symbol), 57. Pair of bytes 195 and 164 are for ä
    It shows that in variable-length encodings like UTF-8, each character can occupy a different number of bytes,
    and to interpret a multi-byte character correctly, we need to consider all preceding bytes in the sequence
    */

    print!("-----------print_nth_char: {}","\n");
    /*
    The object performing a function on current position and advances that position is named iterator
    or cursor. In "std::str::Chars, Chars is a string iterator type
    An iterator is considered to be any expression that has a next method, with no arguments, returning an Option<T> value
    */

    fn print_nth_char(s: &str, mut n: u32) {
        let mut iter: std::str::Chars = s.chars();
        loop {
            let item: Option<char> = iter.next(); // Any iterator has a next function
            // iterator through its 'next' function returns a value of Option<T> type
            match item {
                Some(c) => if n == 0 { println!("{}", c); break; },
                // If n is 0, it prints the current character c, at which point it prints the corresponding character and exits the loop
                // make it !=0 to see a strange result
                None => { break; },
            }
            n -= 1; // This moves the iterator to the next character in the string on the subsequent loop iteration.
        }
    }

    print_nth_char("$€9", 0);
    print_nth_char("$€9", 1);
    print_nth_char("$€9", 2);

    // Printing the character along its numeric code

    print!("{}-----------print_codes: {}", "\n", "\n");
    fn print_codes(s: &str) {
        let mut iter = s.chars();
        loop {
            match iter.next() {
                Some(c) => { println! ("{}: {}", c, c as u32); },
                None => { break;},
            }
        }
    }

    // The Euro sign (€) is assigned the U+20AC is a hexadecimal (base-16) number. When converted to decimal (base-10), it becomes 8364
    // € in Unicode code point is 8364 but in UTF-8 it is represented by 3 bytes 226, 130, 172
    print_codes("$€9");
    

    // All ranges with a starting limit are iterators
    let _v1 = (0u32..10).next();
    let _v2 = (5u32..).next();
    // let _v3 = (..8u32).next(); //Illegal

    print!("-----------print_ite_over_str_byte: {}","\n");
    
    /*
    Both Chars and Bytes are string iterator types, but while the next function of Chars returns the next character of the string, 
    the next function of Bytes returns the next byte of the string. std::str::Chars, std::str::Bytes.
    These string functions are both different from the as_bytes function, which returns a
    slice reference on the bytes of the string. Note: As with python, we can iterate over the bytes of a string:
    */
    fn print_ite_over_str_byte(s: &str) {
        for byte in s.bytes() { // 
            println!("{} ", byte);
        }
    }
    print_ite_over_str_byte("$€9");

}