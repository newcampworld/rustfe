use std::io;

enum BookCategory {
    Fiction(u32),
    Nonfiction(char),
    Textbook(i64, bool),
}

fn main() {
    let mut input = String::new();

    println!("Enter book details in the format: <category> <data>");
    println!("Categories: Fiction, Nonfiction, Textbook");
    println!("For Fiction, enter the page count.");
    println!("For Nonfiction, enter a single character subject code.");
    println!("For Textbook, enter the price (in cents) and 'true' or 'false' for rental availability.");

    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read input");

    let parts: Vec<&str> = input.trim().split_whitespace().collect();

    let book = match parts[0] {
        "Fiction" => {
            let pages: u32 = parts[1].parse().expect("Invalid page count");
            BookCategory::Fiction(pages)
        }
        "Nonfiction" => {
            let subject_code: char = parts[1].chars().next().expect("Invalid subject code");
            BookCategory::Nonfiction(subject_code)
        }
        "Textbook" => {
            let price: i64 = parts[1].parse().expect("Invalid price");
            let is_rental: bool = parts[2].parse().expect("Invalid rental availability");
            BookCategory::Textbook(price, is_rental)
        }
        _ => panic!("Invalid category"),
    };

    match book {
        BookCategory::Fiction(pages) => {
            println!("This is a fiction book with {} pages.", pages);
        }
        BookCategory::Nonfiction(subject_code) => {
            println!("This is a non-fiction book about {:?}", subject_code);
        }
        BookCategory::Textbook(price, is_rental) => {
            if is_rental {
                println!("This textbook is available for rental. Price: ${:.2}", price as f64 / 100.0);
            } else {
                println!("This textbook is for purchase only. Price: ${:.2}", price as f64 / 100.0);
            }
        }
    }
}

/*
We start by importing the io module from the standard library, which provides input/output functionality.
We define the BookCategory enum as before, with variants for Fiction, Nonfiction, and Textbook.
In the main function, we create a mutable String called input to store the user's input.
We print instructions for the user, explaining the expected input format and data for each category.
We use io::stdin().read_line(&mut input) to read the user's input from the standard input stream.
We split the input string into parts using input.trim().split_whitespace().collect(), which creates a Vec<&str> containing the category and data entered by the user.
We use a match expression to create a BookCategory instance based on the user's input:
For Fiction, we parse the second part of the input as a u32 and create a BookCategory::Fiction instance with the page count.
For Nonfiction, we take the first character of the second part of the input as the subject code and create a BookCategory::Nonfiction instance.
For Textbook, we parse the second and third parts of the input as an i64 (price) and a bool (rental availability), respectively, and create a BookCategory::Textbook instance.
We use another match expression to handle the BookCategory instance and print the appropriate message based on the variant and its associated data.
With this code, the user can enter book details in the expected format, and the program will create a BookCategory instance based on the input and print the corresponding message.

For example, if the user enters Fiction 250, the program will create a BookCategory::Fiction instance with a page count of 250 and print "This is a fiction book with 250 pages."

If the user enters Nonfiction S, the program will create a BookCategory::Nonfiction instance with a subject code of 'S' and print "This is a non-fiction book about 'S'."

If the user enters Textbook 1999 true, the program will create a BookCategory::Textbook instance with a price of $19.99 and rental availability set to true, and print "This textbook is available for rental. Price: $19.99."
*/