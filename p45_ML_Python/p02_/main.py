import hashlib

# Get input from the client
input_string = input("Enter a string to hash: ")

# Compute SHA-256 hash
hashed_string = hashlib.sha256(input_string.encode()).hexdigest()

# Output the result
print(f"SHA-256 Hash: {hashed_string}")
