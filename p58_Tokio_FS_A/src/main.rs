/* Asynchronous (async) programming in Rust allows functions to pause and resume without blocking an 
entire operating system thread. Instead of waiting for I/O or other operations to finish, an async
function can yield control to the runtime, which then schedules other async tasks. This is achieved
through the concept of futures: when you mark a function as async fn, it returns a Future that 
is continuously polled by an executor until it completes. Whenever the async function hits an .await,
it suspends if resources (like I/O) are not yet ready, freeing the executor to run other tasks. 
Once those resources become ready (detected by the reactor monitoring OS-level events), 
the executor resumes the previously suspended task right where it left off.

In Rust, one common way to implement this async model is with the Tokio runtime. By annotating 
your main function with #[tokio::main], Tokio provides both the executor (a thread pool) that 
drives all futures and the reactor (an event loop) that notifies the executor about I/O readiness.
Under the hood, non-blocking APIs—like tokio::fs::File for file operations—register with the
reactor so the OS can inform it exactly when a file is ready to read or write. This design enables
efficient concurrency: instead of blocking threads, tasks that must wait simply yield, allowing
other tasks to make progress. */

/* Tokio follows a two-part scheduling model:

Reactor (Event Loop):
    Listens to OS events (e.g., file I/O, timers, sockets).
    When an I/O operation is not ready, the task yields (.await suspends the function).
    When the OS signals that data is ready, the task is resumed.

Executor (Task Polling System):
    Maintains a queue of tasks.
    Runs each task until it reaches .await.
    If a task awaits something, it pauses and the executor runs another task.
    When the awaited operation is ready, the executor resumes that task.

This is why multiple async functions interleave inside a single OS thread without blocking. 

Worksflow is as the followings:
    You write async fn main (with #[tokio::main]).
    Tokio starts up, creating the executor (one or more threads) and the reactor (the OS event loop).
    Your async code (tasks) runs on the executor. When a task hits an I/O operation (socket read, file read, timer, etc.),
    it registers that operation with the reactor, then yields.
    The reactor gets an OS notification (“socket is ready!”). It notifies the executor that the task is now unblocked.
    The executor resumes the previously blocked task, which picks up right after the await.
    This cycle repeats until all tasks are done.
*/

use std::fs::{File as SyncFile};
use std::io::{Read, Write};
use std::time::{Duration, Instant};

use tokio::fs::{File as AsyncFile};             // Async-aware file type from Tokio
/* tokio::fs::File is simply an async-aware file handle (a struct) for reading and writing 
files in a non-blocking manner. It is not an executor or reactor. Instead, it uses the 
executor (the thread pool) and reactor (the OS I/O event loop) under the hood to perform
file operations asynchronously.*/
use tokio::io::{AsyncReadExt, AsyncWriteExt};    // Traits for async reading/writing
use tokio::time;                                 // For async timers / sleeping

const FILE_COUNT: usize = 3; // How many files to create/read.
const LINES_PER_FILE: usize = 999; // How many lines to write in each file.

// SYNC VERSION: uses std::fs, blocks the thread during file operations and sleeps

/* run_sync_fs_stuff():
    - Demonstrates synchronous (blocking) file creation and reading.
    - Uses the standard library's `File`, which blocks until I/O is complete.
    - Thread sleeps with std::thread::sleep, which blocks the thread entirely.

    Observations:
    - If you run many such operations concurrently, you'd typically need multiple threads.
    - The OS will block the thread until disk I/O is finished; no other work can happen on this thread.
*/
fn run_sync_fs_stuff() -> std::io::Result<()> {
    // Start a timer to measure total sync duration
    let start_overall = Instant::now();
    println!("--- Synchronous (blocking) I/O ---");

    // Prepare file names: "sync_file_1.txt", "sync_file_2.txt", etc.
    let filenames: Vec<String> = (1..=FILE_COUNT)
        .map(|i| format!("sync_file_{}.txt", i))
        .collect();

    // 1) CREATE AND WRITE FILES (SYNCHRONOUSLY)
    let start_write = Instant::now();

    for filename in &filenames {
        // Create or overwrite the file
        let mut file = SyncFile::create(filename)?;
        // Write multiple lines
        for line_index in 1..=LINES_PER_FILE {
            writeln!(file, "File: {}, Line: {}", filename, line_index)?;
        }
    }

    let duration_write = start_write.elapsed();
    println!("Sync write took:  {:?}", duration_write);

    // 2) READ FILES (SYNCHRONOUSLY)
    let start_read = Instant::now();
    let mut total_bytes = 0;

    for filename in &filenames {
        // Open the file in read-only mode
        let mut file = SyncFile::open(filename)?;
        let mut contents = String::new();

        // Read the entire file contents
        file.read_to_string(&mut contents)?;

        // Simulate some processing time with a blocking sleep
        std::thread::sleep(Duration::from_millis(50));
        total_bytes += contents.len();
    }

    let duration_read = start_read.elapsed();
    println!("Sync read took:   {:?}", duration_read);
    println!("Total bytes read: {}", total_bytes);

    let total_duration = start_overall.elapsed();
    println!("Sync total took:  {:?}\n", total_duration);

    Ok(())
}

// 2) Asynchronous Version (Concurrent via tokio::spawn)
async fn run_async_fs_stuff() -> std::io::Result<()> {
    let start_overall = Instant::now();
    println!("--- Asynchronous (Tokio) I/O ---");
    // Prepare file names: "async_file_1.txt", "async_file_2.txt", etc.
    let filenames: Vec<String> = (1..=FILE_COUNT)
        /* The 1..=3 range implement the Iterator trait automatically. This means you don't need to 
        explicitly call .iter() because the range itself is inherently iterable */
        .map(|i| format!("async_file_{}.txt", i))
        .collect();

    // 1) CREATE AND WRITE FILES (IN PARALLEL)
    let start_write = Instant::now();
    {
        // We'll collect 'JoinHandle's for each async task we spawn
        let mut tasks = Vec::new();
        for filename in &filenames {
            let file = filename.clone();
            /*  Firstly, tokio::spawn(...) creates a new TASK, which is polled by the Tokio EXECUTOR.
                - The EXECUTOR is a thread pool that runs tasks until they hit an await.
                - If the task awaits on I/O, the task yields, letting the executor run another task.
                - The REACTOR monitors I/O readiness at the OS level.

                Secondly, tokio::spawn always returns a tokio::JoinHandle. Even if you didn't type it out 
                explicitly (let handle: tokio::task::JoinHandle<_> = ...), Rust infers the type automatically.
                If you assign it to a variable (let handle = ...), you explicitly hold the JoinHandle.
                If you don't assign it, the JoinHandle is created but dropped immediately, and the task 
                runs without a handle to await its completion.
                
                The reactor is part of Tokio that interacts with the OS (via epoll, kqueue, IOCP, etc.) 
                to know when file descriptors, network sockets, or timers are ready. If a task is 
                blocked on I/O, the reactor tells the executor as soon as the underlying resource
                is ready. This ensures your code doesn’t block a thread; it just yields until readiness.*/
            let handle = tokio::spawn(async move {
                /* Firstly, using async inside tokio::spawn lets us define and execute the async block, which works 
                like an inline async function, in one step instead of writing a separate function as follows:

                async fn my_task() {
                    // Some async work
                }
                let handle = tokio::spawn(my_task()); // Passing a future to spawn

                Secondly, the move keyword ensures that all variables captured by the async block are moved into it, 
                instead of borrowing them. tokio::spawn runs the async block on a different task, potentially on another thread.
                If the async block borrowed variables from the surrounding scope, they might go out of scope before the async 
                task finishes, leading to use-after-free errors. Using move forces the async block to take ownership of any 
                used variables, making sure they stay valid for the duration of the task. */

                let mut async_file = AsyncFile::create(&file).await?;
                /* If the OS can’t open/create the file immediately, the function yields (pauses) 
                and returns control to the Tokio runtime (executor), letting other tasks run.
                Because these operations are non-blocking, they rely on the Tokio runtime to 
                coordinate their scheduling and I/O readiness checks.
                Async tasks run on executor threads and pause (.await) whenever they must 
                wait on external events. The reactor monitors those external events (I/O readiness), 
                and when something is ready, it wakes the corresponding task so the executor can resume it */

                // Write multiple lines, each of which might .await if the file isn't ready.
                for line_index in 1..=LINES_PER_FILE {
                    async_file.write_all(format!("File: {}, Line: {}\n", file, line_index).as_bytes()).await?;
                        // tokio::io::AsyncWriteExt trait adds the .write_all(...) method for async writes  
                }
                // Return a Result so the spawn handle can propagate errors
                Ok::<(), std::io::Error>(())
            });
            // Collect the JoinHandle to await later
            tasks.push(handle);
        }

        // Wait for all "write" tasks to complete
        for t in tasks {
            // The first .await yields until the spawned task is finished.
            // The second `?` unwraps the Result from the file operations.
            t.await??;
            /* When an async function in Rust, such as AsyncFile::open().await, reaches an .await point, 
            it pauses execution because the operation it is waiting for is not ready yet. At this moment, 
            the function does not directly call the OS epoll API. Instead, the Tokio executor detects 
            that the task is waiting and yields execution, removing it from the active task queue so 
            that the current thread is freed to execute other tasks.

            Once the task yields, the Tokio reactor takes over and registers the file descriptor with 
            the OS using system calls like epoll_ctl(EPOLL_CTL_ADD, fd, ...) on Linux, kqueue() on macOS/BSD, 
            or CreateIoCompletionPort() on Windows. This step ensures that the OS itself monitors 
            the file descriptor without blocking the thread. Since the OS is now responsible for tracking 
            readiness, the thread that was running this async task is never blocked and can continue 
            executing other tasks scheduled by the executor.

            When the OS detects that the file descriptor is ready—whether because the file is successfully opened, 
            data is available for reading, or some other event has occurred—it notifies the Tokio reactor. 
            The reactor then signals the executor that the previously waiting task can resume. 
            The executor picks up the paused function and continues executing it exactly where it 
            left off after the .await. The function never directly interacts with the OS system calls; 
            instead, the reactor and executor coordinate to ensure efficient scheduling and non-blocking behavior. 
            This design allows multiple async tasks to run on a small number of threads, efficiently 
            handling I/O operations without unnecessary thread blocking. */
        }
    }

    let duration_write = start_write.elapsed();
    println!("Async write took: {:?}", duration_write);

    // 2) READ FILES (AGAIN IN PARALLEL)
    let start_read = Instant::now();
    let mut tasks = Vec::new();

    for filename in &filenames {
        let file = filename.clone();

        // Spawn a separate read task for each file
        let handle = tokio::spawn(async move {
            /* tokio::spawn does not create a new OS thread, instead it spawns a new task
            inside Tokio's existing thread pool. These tasks of course shares the same pool

            async fn example() {
                // These will execute one after another, NOT concurrently
                task1().await;
                task2().await;
            }
            */
            // Non-blocking file open
            let mut async_file = AsyncFile::open(&file).await?;

            let mut contents = String::new();
            // Async read into the string buffer
            async_file.read_to_string(&mut contents).await?;

            // We do an async sleep for demonstration; 
            // this yields to the executor while the timer runs in the background
            time::sleep(Duration::from_millis(50)).await;

            // Return how many bytes we read
            Ok::<usize, std::io::Error>(contents.len())
        });

        tasks.push(handle);
    }

    // Now gather the results from each read task
    let mut total_bytes = 0;
    for t in tasks {
        // Wait for the task to finish and add up the bytes read
        total_bytes += t.await??;
    }

    let duration_read = start_read.elapsed();
    println!("Async read took:  {:?}", duration_read);
    println!("Total bytes read: {}", total_bytes);

    let total_duration = start_overall.elapsed();
    println!("Async total took: {:?}\n", total_duration);

    Ok(())
}

/* 3) Asynchronous Version (Sequential, No tokio::spawn)
It does NOT spawn separate tasks. All files are processed in one single async
still non-blocking at the OS level, but no concurrency from multiple tasks). */
 async fn run_async_fs_stuff_no_spawn() -> std::io::Result<()> {
    let start_overall = Instant::now();
    println!("--- Asynchronous (Tokio) I/O [NO spawn, sequential] ---");

    let filenames: Vec<String> = (1..=FILE_COUNT).map(|i| format!("no_spawn_file_{}.txt", i)).collect();

    // 1) CREATE AND WRITE FILES SEQUENTIALLY
    let start_write = Instant::now();
    for filename in &filenames {
        let mut async_file = AsyncFile::create(filename).await?;
        for line_index in 1..=LINES_PER_FILE {
            async_file.write_all(format!("File: {}, Line: {}\n", filename, line_index).as_bytes()).await?;
        }
    }
    /*  The Tokio Runtime is free to schedule other tasks during each await, 
    but inside *this* function, files are still processed in a strict one-at-a-time order.
    
    What is happening here
    Single Async Task, Sequential Flow:
    Your function calls AsyncFile::create(file1).await, waits, writes lines to file1, then moves on to file2.
    There’s no parallel “fan-out”—just a single chain of awaits.
    
    Non-Blocking I/O (Asynchronous):
    Even though it’s sequential, each await call yields if the OS can’t immediately perform the operation.
    The Tokio executor can then run other tasks (e.g., a network server task) while you’re waiting for disk I/O.
    Once the OS signals “ready,” the reactor notifies the executor, which resumes the suspended 
    task exactly where it left off.
    
    No Additional tokio::spawn:
    There is only one async function (run_async_fs_stuff_no_spawn), so we don’t see multiple tasks or 
    concurrency within this loop.
    Async doesn’t automatically mean parallel—it simply means non-blocking.
    
    Hence, the flow is asynchronous (non-blocking) but still sequential in terms of how files 
    are processed. The function will handle file #2 only after finishing file #1, etc. 
    But at each .await, the thread is freed up to do other work if the runtime has other tasks scheduled.
    
    Without tokio::spawn, async functions behave like a single-threaded coroutine: non-blocking but sequential.*/

    /* When an async function does not use tokio::spawn, its internal operations execute sequentially, 
    meaning each step runs only after the previous one completes. However, even though the execution 
    is sequential, the function remains non-blocking at the OS level. This means that when an .await is 
    encountered, the function pauses execution and allows the Tokio runtime to free up the OS thread to handle other tasks.

    For example, imagine an async function that reads a file and then processes its contents:

    async fn read_and_process() {
        let data = read_file().await;  // Pauses here, OS thread is free
         process_data(data).await;      // Continues once read_file() is done
    }

    When read_file().await is reached, the function does not block the OS thread while waiting for the file 
    operation to complete. Instead, the Tokio runtime can schedule other tasks on the same thread—such as handling 
    network requests, running another async function, or performing lightweight computations.

    Now, if tokio::spawn were used inside read_and_process(), tasks could run truly concurrently, utilizing multiple
    execution units (threads or lightweight green threads). But even without tokio::spawn, the key takeaway is 
    that the OS thread remains free to perform other useful work instead of sitting idle while waiting for async operations to complete.

    This design is what makes Rust's async runtime efficient: instead of blocking expensive system resources, it yields
    control back to the runtime and resumes execution only when necessary, allowing maximum utilization of available computing power.*/

    let duration_write = start_write.elapsed();
    println!("Async (no-spawn) write took: {:?}", duration_write);

    // 2) READ FILES SEQUENTIALLY
    let start_read = Instant::now();
    let mut total_bytes = 0;
    for filename in &filenames {
        let mut async_file = AsyncFile::open(filename).await?;
        let mut contents = String::new();
        async_file.read_to_string(&mut contents).await?;

        time::sleep(Duration::from_millis(50)).await; // Async "sleep" but still sequential
     
        total_bytes += contents.len();
    }
    let duration_read = start_read.elapsed();
    println!("Async (no-spawn) read took:  {:?}", duration_read);
    println!("Total bytes read: {}", total_bytes);

    let total_duration = start_overall.elapsed();
    println!("Async (no-spawn) total took: {:?}\n", total_duration);

    Ok(())
}

#[tokio::main]
/* The attribute #[tokio::main] does two things for you automatically:
    - Wraps your main in a Tokio runtime (
        the executor: (thread pool) that polls tasks/futures
        + the reactor: (event loop) that waits for I/O readiness from the OS
    ).
    - Lets you write your main function as async fn main() (the macro transforms it at compile time). */
async fn main() -> std::io::Result<()> {

    run_sync_fs_stuff()?;

    /* When you write multiple async functions and call them inside main(), 
    Tokio automatically tracks them in its executor */

    run_async_fs_stuff().await?;
    run_async_fs_stuff_no_spawn().await?;

    Ok(())

    /* First, tokio::join! does run tasks concurrently, even without tokio::spawn, because tokio::join! 
    awaits multiple async functions at the same time instead of one-by-one.

    async fn task1() {
        println!("Task 1 started");
        sleep(Duration::from_secs(2)).await;
        println!("Task 1 finished");
    }

    async fn task2() {
        println!("Task 2 started");
        sleep(Duration::from_secs(1)).await;
        println!("Task 2 finished");
    }   

    #[tokio::main]
    async fn main() {
        tokio::join!(task1(), task2());
        println!("Both tasks completed!");
    }

    Secondsly, if we implement tokio::spawn inside each, and utilizes tokio::join inside the main function,
    then we have nested concurrency—or concurrency inside concurrency */
}