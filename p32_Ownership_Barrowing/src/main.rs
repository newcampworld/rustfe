/*
Rust's ownership model ensures that only one owner exists at a time, 
but you can return the ownership of a value to the caller in certain ways.

In C++, heap allocation is both deterministic and explicit:
    Deterministic: Allocation occurs at clearly defined points in the source code, 
    as specified by the programmer.
    Explicit: Deallocation requires direct invocation of functions like free or delete.

In Java, heap allocation is both nondeterministic and implicit:
    Nondeterministic: Allocation and deallocation occur at unspecified times during 
    program execution, managed by the garbage collector.
    Implicit: Programmers do not need to write explicit deallocation statements; 
    memory management is handled automatically.

In Rust, heap allocation is both deterministic and implicit, thanks to its ownership model:
    Deterministic: Allocation and deallocation occur at well-defined points, directly 
    tied to an object’s lifetime as determined by ownership rules.
    Implicit: Programmers do not need to explicitly manage memory with deallocation statements; 
    Rust’s ownership and borrowing system ensures safe and automatic memory management.

The rules for deterministic and explicit/implicit memory management generally apply to heap allocation 
because the heap requires manual or runtime-based management. For stack allocation, both C++ and Java 
handle allocation and deallocation implicitly and deterministically, making stack memory easier and 
faster to manage. Rust follows a similar approach to C++ for stack memory but extends the concept of 
deterministic deallocation to the heap through its ownership model.
*/

fn main() {

    let s1 = String::from("Hello");
    take_ownership(s1); // Ownership of `s` is moved to `take_ownership` function
    // println!("{}", s); // This will cause an error because 's' ownership was transferred

    let mut s2 = String::from("Hello");
    modify_string(&mut s2); // `s` is barrowed mutably with `&mut`, allowing the function to modify it
    println!("After modification: {}", s2); // `s` can still be used, because ownership was not transferred
}

fn take_ownership(s: String) {
    println!("Ownership taken: {}", s);
}
/*
Ownership Transfer (without &)
When ownership of a value is transferred by passing it to a function without using a reference (&), 
the ownership moves from the caller to the called function. Once transferred, the original variable 
in the caller's scope becomes invalid, and any attempt to access it results in a compiler error. 
This behavior ensures memory safety by preventing multiple mutable owners or dangling references. 
Ownership can be regained by returning the value from the called function, effectively transferring 
it back to the caller as in the following code:

fn take_ownership(s: String) -> String {
    println!("Ownership taken: {}", s);
    s // Return ownership to the caller
}
*/

fn modify_string(s: &mut String) {
    s.push_str(", world!");
}
/*
Barrowing
When you borrow a value using & (for immutable borrowing) or &mut (for mutable borrowing), 
the ownership is not transferred. Instead, the function gets temporary access to the value, 
while the original owner (the caller) retains ownership.  After the borrowed value is used in 
the called function, the original value can still be used in the caller but under certain conditions:

    - If borrowed immutably (&), the original value can be used but not modified.
    - If borrowed mutably (&mut), the original value cannot be accessed until the borrowing is done 
    (to ensure no simultaneous mutable access).

Borrowing provides a safe way to give access to data without transferring ownership.
Ownership is retained by the caller; the called function only gets a reference to the value.
*/