fn main() {

    // Move semantics
    let v1 = vec![11, 22, 33];
    #[allow(unused_variables)]
    let v2 = v1; // The ownership of v1 is moved to v2. After this line, v1 is no longer valid 
    // println!("{} ", v1.len()); // compile-time error because v1's ownership has been moved to v2
    /*
    The header of v1 is copied onto the header of v2, and nothing else happens. Subsequently, 
    v2 can be used, and it refers to the heap buffer that was allocated for v1, but v1 cannot
    be used anymore. This is implemented, by default, by Rust and is called Move semantics.
    Refere to page 359-361 distinguishing share semantics, in Java, and copy semantics, in C++.

    Move semantics is used, by default, by dynamic strings, boxes, 
    any collection (including vectors), enums, structs, and tuple-structs.
    */

    // Copy semantics
    let v3 = vec![11, 22, 33];
    let _v4 = v3.clone(); // creates a deep copy of the vector, duplicating the data in the heap.
    // Both v3 and v4 now own independent copies of the data.
    println!("Length of v3 which is cloned: {}", v3.len());
    let i1 = 9; let i2 = i1; let s1 = "denise"; let s2 = s1; let r1 = &i1; let _r2 = r1;
    // primitive numbers, static string, & references are being copy semantics used
    println!("Copy!: {} {} {}", i1, s1, r1);
}