use futures::future::join_all; // join_all() takes an iterator of async tasks (futures) and runs them concurrently
use tokio::fs::File as AsyncFile;
use tokio::io::{AsyncWriteExt, AsyncReadExt};
use tokio::time::{self, Duration};
use std::io;

const FILE_COUNT: usize = 3;
const LINES_PER_FILE: usize = 10;

async fn run_async_fs_stuff() -> io::Result<()> {
    let filenames: Vec<String> = (1..=FILE_COUNT).map(|i| format!("async_file_{}.txt", i)).collect();

    println!("Starting concurrent file writes...");

    // Concurrent file writes
    let write_tasks = filenames.iter().map(|filename| {
        // write_tasks is not a Vec<T>. Instead, it's an iterator that lazily produces async blocks (futures)
        async move {
            let mut async_file = AsyncFile::create(filename).await?;
            for line_index in 1..=LINES_PER_FILE {
                async_file.write_all(format!("File: {}, Line: {}\n", filename, line_index).as_bytes()).await?;
            }
            println!("Finished writing: {}", filename);
            Ok::<(), io::Error>(())
        }
    }); // end of map :). We name this block as A

    /* Without using the join_all(), block A is totally sequential where both creating the files and 
    writing into the files are done one by one and completes before moving to the next however non-blocking
    in which once the oprtation hits await it yields the control back to the Tokio executor, 
    allowing other async tasks. The thread is not locked like in synchronous code, meaning other tasks (such as
    an async web server or a different task in the same program) can still make progress. This version is 
    non-blocking but not concurrent because each iteration must fully finish before the next one starts. 
    So if you had a web server running on another thread, it would still be responsive while waiting on disk I/O. 
    But the files themselves would still be written one by one.  By "other tasks," it means any other async operations
    scheduled in the Tokio runtime—not necessarily something external or unrelated to the Rust program. 
    
    When .await is hit inside this loop, Tokio has no other tasks to run, so the thread remains idle while waiting
    for the I/O operation to complete. The I/O itself is non-blocking, meaning it does not lock the thread, 
    but because nothing else was scheduled alongside it, there is no concurrency—each file operation must 
    fully finish before the next one begins. This happens because the loop sequentially starts one file operation
    at a time, and when .await is hit, there are no other file operations already in progress to switch to.

    By using join_all(), multiple file operations are scheduled at the same time, allowing them to progress concurrently.
    In this case, when one task reaches .await, instead of waiting idly, Tokio immediately switches to another pending
    file operation that is ready to continue. This ensures that multiple file operations can make progress
    simultaneously, making full use of the asynchronous runtime.

    Having .await inside a loop without join_all() is functionally meaningless in terms of concurrency.
    If no other tasks are scheduled alongside it, .await will only introduce non-blocking behavior but will not
    enable true parallel progress. To fully leverage asynchronous execution, we must not forget to use join_all()
    when dealing with multiple independent tasks. */

    // Await all write tasks concurrently
    let write_results = join_all(write_tasks).await;
    // join_all() consumes the iterator write_tasks, executing all async tasks concurrently
    for (i, result) in write_results.into_iter().enumerate() {
        if let Err(e) = result {
            eprintln!("Error writing to {}: {}", filenames[i], e);
        }
    }

    println!("Starting concurrent file reads...");

    // Concurrent file reads
    let read_tasks = filenames.iter().map(|filename| {
        async move {
            match AsyncFile::open(filename).await {
                Ok(mut async_file) => {
                    let mut contents = String::new();
                    if let Err(e) = async_file.read_to_string(&mut contents).await {
                        eprintln!("Error reading {}: {}", filename, e);
                        return Err(e);
                    }
                    time::sleep(Duration::from_millis(50)).await;
                    println!("Read completed: {}", filename);
                    Ok(contents.len())
                }
                Err(e) => {
                    eprintln!("Failed to open file {}: {}", filename, e);
                    Err(e)
                }
            }
        }
    });

    // Await all read tasks concurrently and sum the results
    let read_results = join_all(read_tasks).await;
    let total_bytes: usize = read_results.into_iter().filter_map(Result::ok).sum();

    println!("Total bytes read: {}", total_bytes);

    Ok(())
}

#[tokio::main]
async fn main() -> io::Result<()> {
    run_async_fs_stuff().await?;
    Ok(())
}