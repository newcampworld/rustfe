// Generic Function Monomorphization (Instantiation)

/*
The Following C++ code is a function template
#include <iostream>
#include <cmath>
template <typename Number>

Number quartic_root(Number x) {
    return sqrt(sqrt(x));
}

int main() {
    
    // Both first instantiations check that, for the float and for the double concrete types,
    // there is a sqrt function that is application to them and it succeeds
    std::cout << quartic_root((float)100);
    std::cout << quartic_root((double)100);
    
    // Compiler fails to instantiate the function for the const char*, 'Hello', due to an implicit conversation. And,
    // the error message is somewhat obscure because it talks about vars, func, belong to the implementation of the
    // library, not to its interface. 
    std::cout << quartic_root("Hello");

    // more details about API knowledge is not enough but the whole library implementation is necessary at page 280
}
*/

/*

In Rust, yet, the following code is illegal. Because, in the expression x.sqrt(), where the expression x is 
of the Number generic type, such type doesn’t have the sqrt method. Actually, the type Number has just
been defined, so it has pretty much no methods.

fn quartic_root<Number>(x: Number) -> Number {
    x.sqrt().sqrt() // ILLEGAL: no sqrt method for Number
}
print!("{} {}",
    quartic_root(100f64),
    quartic_root(100f32));

That generic function replaces the following two functions however as described faced that issue:

fn quartic_root_f64(x: f64) -> f64 { x.sqrt().sqrt() }
fn quartic_root_f32(x: f32) -> f32 { x.sqrt().sqrt() }

*/

/*
In Rust, the solution to prevent incorrect invocations of a generic function involves defining an abstract capability, 
known as a trait, that the argument types must satisfy. If the function is called with an argument whose type meets the 
trait's requirements, it ensures that the function can be monomorphized with that type. Conversely, if the argument's 
type does not satisfy the trait, the error message will explain why the call is invalid.
*/

// A trait in Rust is essentially a set of method signatures that a type must implement.
trait HasSquareRoot { // trait declaration
    fn sq_root(self) -> Self;
}
/*
In Chapter 18, we saw how the impl keyword, followed by the name of a type, was
used to declare associated functions and methods for the specified type.

Every impl block used to implement a trait must have the same signatures of the trait it is
implementing: not one more, not one less, not one different.
*/
impl HasSquareRoot for f32 { // an implementation of the trait for type f32 - 32-bit floating-point number
    // the capability to invoke the sq_root method is given to f32 type 
    fn sq_root(self) -> Self {self.sqrt()}
    // the signatures of the methods contained in the impl statements must be
    // the same as the signature contained in the previous trait statement
}
impl HasSquareRoot for f64 {
    // type f64 provides a concrete implementation for the sq_root method, thus satisfying the HasSquareRoot trait
    fn sq_root(self) -> Self {self.sqrt() }
}

fn quartic_root<Number>(x: Number) -> Number // declaration of generic function
where Number: HasSquareRoot { // trait bound: it means that the Number generic type must implement the HasSquareRoot trait
    x.sq_root().sq_root()
    /*
    For the code that invokes the method, such where clause means: when invoking this
    method, you must ensure that the type you pass for the Number type parameter implements
    the HasSquareRoot trait.
    */
}
fn main() {
    println!("\n{} \n{}",
        /*
        sq_root method can be invoked on types f32 and f64 because they has implementation for the trait
        if you replace the argument with true, then the type of that expression, that is bool, does not implement the
        HasSquareRoot trait, so you are violating the contract. Indeed, you would get the
        compilation error: the trait bound `bool: HasSquareRoot` is not satisfied.
        ***** Also, not even implemented to i32, regardless of the reasonableness of the operation: quartic_root(81i32)
        */
        quartic_root(100f64),
        quartic_root(100f32));
        // since both expressions implement the trait, they are valid arguments
}
