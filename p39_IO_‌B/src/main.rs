use std::io;
use rpassword::read_password;

fn main() {
    let name1 = String::from("adib ");
    println!("First string: {}", name1);
    
    let mut name2 = String::new();
    println!("Please enter another string:");
    io::stdin().read_line(&mut name2).expect("Failed to read line");
    let name2 = name2.trim();
    println!("Second string: {}", name2);

    /*
    The characters are not added to name2 one by one as you type them. 
    Instead, the input is buffered. When you press the Enter key 
    (i.e., after you've finished typing the entire sentence), the whole line is added to name2 in one go.
    */

    /*
    Without &mut: name2 is initialized as an empty String with String::new(). 
    A String in Rust is stored on the heap and can grow dynamically.
    read_line appends the input to the existing contents of name2. 
    So if name2 is empty, it will now contain the entered string.
    Without &mut, the read_line function wouldn't have permission to modify the existing name2 string. 
    Rust's ownership and borrowing rules ensure that read_line can directly 
    modify the string in place without creating a new copy.
    The string itself (name2) is modified in place. Rust doesn't allocate a new string after you input; 
    it simply writes the input into the existing memory allocated for name2.
    */

    /*
    Using an Array []: Rust arrays have fixed sizes, meaning they cannot dynamically grow to 
    accommodate varying input lengths. 
    This makes arrays unsuitable for handling input from read_line, 
    which typically stores data in a dynamic container like String or Vec. 
    You would also encounter a type mismatch error because read_line expects a 
    mutable String or something that can grow dynamically.
    */

    /*
    Using a Vec<u8>: A Vec<u8> (vector of bytes) could work, but you would need to 
    manually handle the conversion between u8 and characters
    The user types input, and each key they press (including any spaces, punctuation, or special characters) 
    is captured as raw bytes and stored in the Vec<u8>.
    In a Vec<u8>, each character (or more precisely, each byte) of the input is stored as an individual element of the vector
    After reading the input, the program converts the bytes into a proper String.

    let mut input: Vec<u8> = Vec::new(); // A vector to store input bytes
    ...
    let input_str = String::from_utf8(input).expect("Invalid UTF-8 input");


    */
    
    let mut name3 = String::new();
    println!("Please enter another string:");
    io::stdin().read_line(&mut name3).expect("Failed to read line");
    let name3 = name3.trim();
    println!("Third string: {}", name3);

    // to get input without displaying it on the terminal 
    println!("Please enter your password:");
    let password = read_password().expect("Failed to read password");

    println!("Your password is: {}", password); // Note: This would be hidden.
}