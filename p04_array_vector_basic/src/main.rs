fn main () {

    println!("Multidemensional Array: ");
    let d = [[[0;4];4];4];
    for i in 0..4 {
        for j in 0..4 {
            for k in 0..4 {
                println!("At [{}][{}][{}] we have {}" ,i ,j ,k , d[i][j][k]);
            }
        }
    }

    // array cannot contain objects of different types

    #[allow(unused_variables)]
    let d = [[[6];7];9];

    /*
    The usize type is allowed as an index of an array. 
    Notice that it is not allowed even to use an index of u16 type on a 16-bit system, nor
    to use an index of u32 type on a 32-bit system, nor to use an index of u64 type on a 64-bit
    system. This guarantees source code portability.
    */
    
    println!("Vector: ");
    let mut v = vec![0xFF, 0xEA, 0x55, 0x4A];
    println!("size is now: {}", v.len());
    for i in 0..v.len() {
        println!("at {}: {:x}", i, v[i]);
    }
    v.push(0xB1);
    v.insert(1, 0x22);
    v.remove(3);
    println!("size is now: {}", v.len());
    for i in 0..v.len() {
        println!("at {}: {:x}", i, v[i]);
    }

    // Vectors allow us to do everything that is allowed for arrays, but they allow also us to
    // change their size after they have been initialized. Hence, changing the size is matter not midification

    // 1. vec![] macro
    let v_macro = vec![0xFF, 0xEA, 0x55, 0x4A];
    println!("v_macro: {:?}", v_macro);

    // 2. Vec::new()
    let mut v_new = Vec::new(); // type inference might need a hint: Vec<u8>
    v_new.push(0xAA);
    v_new.push(0xBB);
    println!("v_new: {:?}", v_new);

    // 3. Vec::with_capacity()
    let mut v_capacity = Vec::with_capacity(4);
    v_capacity.push(0x11);
    v_capacity.push(0x22);
    v_capacity.push(0x33);
    v_capacity.push(0x44);
    println!("v_capacity: {:?}", v_capacity);

    // 4. Using collect on an iterator
    let v_collect: Vec<u8> = (0..4).collect();
    println!("v_collect: {:?}", v_collect);

    // 5. Using Vec::from or .to_vec()
    let arr = [0xDE, 0xAD, 0xBE, 0xEF];
    let v_from_arr = Vec::from(arr);
    println!("v_from_arr: {:?}", v_from_arr);

    let str_bytes = "Hello";
    let v_from_str = str_bytes.as_bytes().to_vec();
    println!("v_from_str: {:?}", v_from_str);
    // 1. vec![] macro
    let v_macro = vec![0xFF, 0xEA, 0x55, 0x4A];
    println!("v_macro: {:?}", v_macro);

    // 2. Vec::new()
    let mut v_new = Vec::new(); // type inference might need a hint: Vec<u8>
    v_new.push(0xAA);
    v_new.push(0xBB);
    println!("v_new: {:?}", v_new);

    // 3. Vec::with_capacity()
    let mut v_capacity = Vec::with_capacity(4);
    v_capacity.push(0x11);
    v_capacity.push(0x22);
    v_capacity.push(0x33);
    v_capacity.push(0x44);
    println!("v_capacity: {:?}", v_capacity);

    // 4. Using collect on an iterator
    let v_collect: Vec<u8> = (0..4).collect();
    println!("v_collect: {:?}", v_collect);

    // 5. Using Vec::from or .to_vec()
    let arr = [0xDE, 0xAD, 0xBE, 0xEF];
    let v_from_arr = Vec::from(arr);
    println!("v_from_arr: {:?}", v_from_arr);

    let str_bytes = "Hello";
    let v_from_str = str_bytes.as_bytes().to_vec();
    println!("v_from_str: {:?}", v_from_str);    
}