pub fn root_function() {
    println!("Called root_function");
}

mod grandparent {
    pub fn grandparent_function() {
        println!("Called grandparent_function");
    
        // There is no way to specify that an identifier is private; it is just the default.
        fn logic() {
            println!("I am the King!")
        }
    }

    pub mod parent {
        pub fn parent_function() {
            println!("Called parent_function");
        }

        pub mod child {
            // Function in the child module that calls the function from the grandparent module
            pub fn child_function() {
                // Use `super::super` to move up two levels to the `grandparent` module
                super::super::grandparent_function();
                // super: Moves up one level in the module hierarchy.
                // super::super: Moves up two levels.
                crate::root_function();
                // crate:: can be used to directly reference functions, structs, or other items 
                // defined at the root level of your crate from any nested module.
            }
        }
    }
}

fn main() {
    grandparent::grandparent_function();
    grandparent::parent::child::child_function();
}
