fn main() {
    // Infallible Type converter using trait Into
    /*
    struct LargeNumber1 (f64);
    struct SmallNumber1 (f32);

    let ln = LargeNumber1 (1. / 3.);
    let sn = ln as SmallNumber1;

    print!("{}", sn.0) // as keyword is not allowed to convert between nonprimitive types. The solution is as follows:
    */

    println!("Real Division is: {}", 1./3.);
    struct LargeNumber (f64);
    struct SmallNumber (f32);

    impl Into<SmallNumber> for LargeNumber {
        fn into(self) -> SmallNumber {
            /*
            The into method takes self, which is a LargeNumber, and returns a SmallNumber by casting the f64 inside LargeNumber to an f32
            */
          SmallNumber (self.0 as f32)
       }
       // drawback of this trait is that it requires us to call the into function in a context in which the destination type can be inferred (see A)
    }
    let ln = LargeNumber (1. / 3.);
    let sn: SmallNumber = ln.into();
    // A
    /*
    The into() method doesn't know what type to convert ln into unless it’s explicitly told or can infer it from the context.
    In the line above, the type of sn is explicitly annotated as SmallNumber, so the compiler knows to convert ln into a SmallNumber.
    let sn = ln.into(); // Error: Rust can't infer what type ln should be converted into

    */
    println!("Infallible Division by Intro is: {}", sn.0);

    // Infallible Type converter using trait From

    struct LargeNumber2 (f64);
    struct SmallNumber2 (f32);

    impl From<LargeNumber2> for SmallNumber2 {
        fn from(source: LargeNumber2) -> Self {
            Self (source.0 as f32)
        }
    }
    let ln2 = LargeNumber2 (1. / 3.);
    let sn2 = SmallNumber2::from(ln2); // Type inference is not required by compiler, because the destination type is specified anyway
    println!("Infallible Division by From is: {}", sn2.0);

    /*
    Infallible Conversions (From/Into): The conversion will always succeed, so there's no need to handle errors.
    Fallible Conversions (TryFrom/TryInto): The conversion might fail, requiring you to handle the possibility of an error with a Result.
    */
}
