/*
In this version we introduced a sleep in the code to pause execution after opening the file but before the program 
ends. This will give you time to issue the lsof command in a separate terminal to check if the file is indeed open.
*/

use std::fs::File;
use std::io::{self, Write};
use std::thread::sleep;
use std::time::Duration;

struct Logger {
    file: File,
}

impl Logger {
    fn new(filename: &str) -> io::Result<Logger> {
        let file = File::create(filename)?;
        println!("File {} opened successfully.", filename);
        Ok(Logger { file })
    }

    fn log(&mut self, message: &str) {
        writeln!(self.file, "{}", message).expect("Failed to write to log file");
    }
}

fn main() {
    {
        let mut logger = Logger::new("log.txt").expect("Failed to create log file");
        logger.log("This is a log message.");

        // Sleep for 10 seconds to allow time to check the open file with lsof
        println!("Sleeping for 10 seconds...");
        sleep(Duration::new(10, 0));
    } // Logger goes out of scope here, and the file should be closed

    println!("Logger has been dropped, and the file is closed.");
}

// Here is the result:
// shell #1

/*
[pshko@localhost p28_oop_b]$ cargo run
   Compiling main v0.1.0 (/home/pshko/rust/rustfe/p28_oop_b)
    Finished `dev` profile [unoptimized + debuginfo] target(s) in 0.34s
     Running `target/debug/main`
File log.txt opened successfully.
Sleeping for 10 seconds...
Logger has been dropped, and the file is closed.
[pshko@localhost p28_oop_b]$ 
*/

// shell #2

/*
[pshko@localhost ~]$ lsof | grep log.txt
[pshko@localhost ~]$ lsof | grep log.txt
main      136090                           pshko    3w      REG              253,2        23   17660113 /home/pshko/rust/rustfe/p28_oop_b/log.txt
[pshko@localhost ~]$ lsof | grep log.txt
[pshko@localhost ~]$ 

*/