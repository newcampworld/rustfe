/* 
Rust iterators are lazy, meaning they don't perform any computations until they're explicitly consumed.
Iterator Trait: The core of iterators in Rust is the Iterator trait, which is defined as follows:
pub trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;
    // Other methods...
}
The next method advances the iterator and returns Some(item) if there is an item, or None if the iterator is exhausted.
*/

fn main() {

/*
---------------- into_iter() 
It consumes the collection and takes ownership of the values.
- This method moves the ownership of each item from the collection to the loop variable.
- The collection will no longer be usable after `into_iter()` is called, as its ownership has been transferred.
- The loop variable (e.g., `item`) will hold the values directly, allowing them to be modified if mutable.
*/
fn into_iter_owned() {
    let v = vec![10, 20, 30];
    for mut item in v.into_iter() { //each item is taken its ownership and needs to get modified in the loop
        item += 1;
        println!("{}", item); // prints 11, 21, 31
    }
    // `v` is no longer usable here, as it has been moved; so this fails: println!("{:?}", v);
}
/*
Example Explained:
Consumes the collection: This means the original collection (e.g., Vec<T>) is no longer accessible after
into_iter() is called. The items are moved from the collection to the loop variable, taking full ownership.
Moves ownership: The values themselves are owned by the loop variable, so you can mutate 
them if declared as mut (like in mut item in your code).
After into_iter(): The collection cannot be accessed again because Rust's ownership rules 
prevent using moved values. This ensures that the program avoids dangling references or double frees.
*/

/*
---------------- iter(): 
Creates an iterator that borrows the values.
- The collection remains accessible after the iteration because the iterator borrows each item.
- The loop variable is a reference to the item (`&T`), and you must dereference it to access or modify the value.
- Since the items are borrowed, you cannot modify them directly unless they are mutable references.
*/
fn iter_borrowed() {
    let v = vec![10, 20, 30];
    let vec_ref_iterator = v.iter();
    for item_ref in vec_ref_iterator {
        println!("{}", *item_ref + 1); // prints 11, 21, 31
    }
    // `v` is still usable here because `iter()` only borrows the items
}
/*
Example Explained:
iter() (Borrows the Values):
Borrows items: The iter() method creates an iterator that borrows each item from the collection (&T). 
This allows you to iterate over the collection without consuming it,
leaving the collection intact and usable afterward.
Loop variable holds a reference: Since the loop variable holds a reference (&T),
you'll need to dereference it (*item_ref) to access or modify the underlying value.
Read-only by default: The default behavior is read-only. You can’t modify the items
unless you iterate with mutable references using iter_mut().
*/

/*
---------------- iter_mut():
It creates an iterator that mutably borrows each item in the collection (&mut T), 
allowing you to modify the items as you iterate through them.
Loop Variable Holds Mutable References: When you use iter_mut(), the loop variable holds a mutable 
reference to each item, meaning you can modify the values directly without needing to 
return ownership or copy the values.
The Collection Remains Usable: After iterating with iter_mut(), the collection is still available, 
but any changes you made to the elements during iteration will be reflected in the original collection.
*/
fn iter_mut_example() {
    let mut v = vec![10, 20, 30];  // Declare a mutable vector
    for item in v.iter_mut() {     // Use iter_mut() to get mutable references
        *item += 1;                // Modify each item by adding 1
    }
    println!("{:?}", v);           // Outputs: [11, 21, 31]
}
/*
Example Explained:
Mutable Borrow: The iterator created by iter_mut() yields mutable references (&mut T), so each item 
in the loop is a mutable reference to an element in the collection.
Direct Mutation: Using *item allows you to dereference the mutable reference and modify the value it points to. 
The changes happen in place in the original collection.
Collection is Usable Afterward: Since the items are only mutably borrowed, you can continue using the 
collection after the iteration is complete, and any changes will persist.
*/

/*
Key Takeaways:
iter(): When you just need to read from the collection.
iter_mut(): When you need to modify the elements of the collection.
into_iter(): When you want to take ownership of the collection's elements and don’t need the collection afterward.
*/

}