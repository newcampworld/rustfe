use std::error::Error;
use trust_dns_resolver::Resolver;
use trust_dns_resolver::config::{ResolverConfig, ResolverOpts};
use trust_dns_resolver::config::LookupIpStrategy;
use trust_dns_resolver::proto::rr::RecordType;

fn lookup(name: &str) -> Result<(), Box<dyn Error>> {
    let resolver = Resolver::new(ResolverConfig::default(), ResolverOpts::default())?;

    let query_types = vec![RecordType::A, RecordType::AAAA, RecordType::CNAME, RecordType::MX, RecordType::NS];

    for qtype in query_types {
        let response = resolver.lookup(
            name,
            qtype,
        )?;

        if !response.is_empty() {
            print!("{:?}", response);
        }
    }

    Ok(())
}

fn main() {
    if let Err(err) = lookup("fortinet.com") {
        eprintln!("Failed to perform DNS lookup: {}", err);
    }
}
