use std::env;

fn main() {
    // Parse command-line arguments
    let args: Vec<String> = env::args().collect();

    // Check if the user provided the correct number of arguments
    if args.len() < 2 {
        eprintln!("Usage: {} <squ|exp>", args[0]); // args[0] is the path
        return;
    }

    let operation = &args[1]; // either "squ" or "exp" is captured as the first argument passed

    match operation.as_str() {
        "squ" => calculate_square(),
        "exp" => calculate_exponentiation(),
        _ => eprintln!("Invalid option. Please use 'squ' or 'exp'."),
    }
}

// Function to calculate N^2
fn calculate_square() {
    let mut n: u64 = 1;

    loop {
        if let Some(nn) = n.checked_mul(n) {
            /*
            checked_mul is a method that performs multiplication with overflow checking.
            If the multiplication overflows (i.e., exceeds the maximum value that u64 can hold),
            it returns None. Otherwise, it returns Some(nn) where nn is the result of the multiplication.
            */
            println!("{}^2 = {}", n, nn);
        } else {
            break;  // Break if overflow is detected
        }
        n += 1;
    }
}

// Function to calculate N^N
fn calculate_exponentiation() {
    let mut n: u64 = 1;

    loop {
        // Calculate N^N (n raised to the power of n) and check for overflow
        if let Some(nn) = n.checked_pow(n as u32) {
            println!("{:<3} ^ {:<3} = {}", n, n, nn);
        } else {
            break;  // Break if overflow is detected
        }
        n += 1;
    }
}
