fn main(){

    use std::io::Read;
    use std::io::Write;

    // reading and writing byte at a time
    fn byte_reader_writer (){

        let mut command_line: std::env::Args = std::env::args();
        command_line.next().unwrap(); // it skips the name of this file
    
        let source = command_line.next().unwrap(); // passing the source file path
        let destination = command_line.next().unwrap(); // passing the destination file path
    
        let mut file_in = std::fs::File::open(source).unwrap(); // opening the file and the new handle is assigned to the file_in variable
        let mut file_out = std::fs::File::create(destination).unwrap();
    
        let mut buffer = [0u8; 4096]; // a 4096-byte buffer is allocated in the stack
    
        loop { // this loops repeatedly reads a 4096-byte chunk from the source file and writes it to the output
            let nbytes = file_in.read(&mut buffer).unwrap(); // number of bytes read is implicitly specified by the length of the buffer
            file_out.write_all(&buffer[..nbytes]).unwrap();
            // It creates a slice that includes only the first nbytes elements of buffer
            if nbytes < buffer.len() { break; } // if the number of bytes read was less than the length of the buffer, the loop is terminated
        }
    
        /*
        no need to explicitly close the files because as soon as the file handles exit thier scopes, 
        the files are automatically closed, saving and releasing all internall temporary buffers; as opposed to C++
        */
    }

    // byte_reader_writer ()
    // How to run: ""./target/debug/main a b"

    // sometimes it is more convenient to process it a line at a time.
    fn line_reader_writer(){

        let mut command_line = std::env::args();
        command_line.next();
        let pathname = command_line.next().unwrap();
        let counts = count_lines(&pathname).unwrap();
        println!("file: {}", pathname);
        println!("n. of lines: {}", counts.0);
        println!("n. of empty lines: {}", counts.1);

        fn count_lines(pathname: &str)
        -> Result<(u32, u32), std::io::Error> {
            use std::io::BufRead;
            let f = std::fs::File::open(pathname)?;
            let f = std::io::BufReader::new(f);
            let mut n_lines = 0;
            let mut n_empty_lines = 0;
            for line in f.lines() {
                n_lines += 1;
                if line?.trim().len() == 0 {
                    n_empty_lines += 1;
                }
            }
        Ok((n_lines, n_empty_lines))
    }

    }

    // line_reader_writer()

}

