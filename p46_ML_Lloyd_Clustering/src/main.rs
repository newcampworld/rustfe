use rand::seq::SliceRandom;
use rand::thread_rng;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
struct Point {
    latitude: f64,
    longitude: f64,
}

#[derive(Debug, Clone)]
struct Cluster {
    center: Point,
    points: Vec<Point>,
}

impl Cluster {
    fn new(center: Point) -> Self {
        Self {
            center,
            points: Vec::new(),
        }
    }

    fn update_center(&mut self) {
        let total_points = self.points.len();
        if total_points > 0 {
            let sum_lat: f64 = self.points.iter().map(|p| p.latitude).sum();
            let sum_lon: f64 = self.points.iter().map(|p| p.longitude).sum();
            self.center = Point {
                latitude: sum_lat / total_points as f64,
                longitude: sum_lon / total_points as f64,
            };
        }
    }
}

fn euclidean_distance(p1: Point, p2: Point) -> f64 {
    ((p1.latitude - p2.latitude).powi(2) + (p1.longitude - p2.longitude).powi(2)).sqrt()
}

fn lloyds_algorithm(points: &[Point], k: usize) -> Vec<Cluster> {
    let mut rng = thread_rng();
    let mut clusters: Vec<Cluster> = points
        .choose_multiple(&mut rng, k)
        .map(|&center| Cluster::new(center))
        .collect();

    loop {
        // Clear points from each cluster
        for cluster in &mut clusters {
            cluster.points.clear();
        }

        // Assign points to the nearest cluster
        for &point in points {
            let mut nearest_cluster = 0;
            let mut min_distance = euclidean_distance(point, clusters[0].center);

            for (i, cluster) in clusters.iter().enumerate().skip(1) {
                let distance = euclidean_distance(point, cluster.center);
                if distance < min_distance {
                    nearest_cluster = i;
                    min_distance = distance;
                }
            }
            clusters[nearest_cluster].points.push(point);
        }

        // Save current centers for convergence check
        let previous_centers: Vec<Point> = clusters.iter().map(|c| c.center).collect();

        // Update cluster centers
        for cluster in &mut clusters {
            cluster.update_center();
        }

        // Check for convergence
        let converged = clusters
            .iter()
            .zip(&previous_centers)
            .all(|(cluster, &prev_center)| {
                euclidean_distance(cluster.center, prev_center) < 1e-6
            });

        if converged {
            break;
        }
    }

    clusters
}

fn main() {
    // Latitude and longitude of all US state capitals
    let state_capitals = vec![
        Point { latitude: 32.377716, longitude: -86.300568 }, // Montgomery, AL
        Point { latitude: 58.301598, longitude: -134.420212 }, // Juneau, AK
        Point { latitude: 33.448143, longitude: -112.096962 }, // Phoenix, AZ
        Point { latitude: 34.746613, longitude: -92.288986 }, // Little Rock, AR
        Point { latitude: 38.576668, longitude: -121.493629 }, // Sacramento, CA
        Point { latitude: 39.739227, longitude: -104.984856 }, // Denver, CO
        Point { latitude: 41.764046, longitude: -72.682198 }, // Hartford, CT
        Point { latitude: 39.157307, longitude: -75.519722 }, // Dover, DE
        Point { latitude: 30.438118, longitude: -84.281296 }, // Tallahassee, FL
        Point { latitude: 33.749027, longitude: -84.388229 }, // Atlanta, GA
        Point { latitude: 21.307442, longitude: -157.857376 }, // Honolulu, HI
        Point { latitude: 43.617775, longitude: -116.199722 }, // Boise, ID
        Point { latitude: 39.798363, longitude: -89.654961 }, // Springfield, IL
        Point { latitude: 39.768623, longitude: -86.162643 }, // Indianapolis, IN
        Point { latitude: 41.591087, longitude: -93.603729 }, // Des Moines, IA
        Point { latitude: 39.048191, longitude: -95.677956 }, // Topeka, KS
        Point { latitude: 38.186722, longitude: -84.875374 }, // Frankfort, KY
        Point { latitude: 30.457069, longitude: -91.187393 }, // Baton Rouge, LA
        Point { latitude: 44.307167, longitude: -69.781693 }, // Augusta, ME
        Point { latitude: 38.978764, longitude: -76.490936 }, // Annapolis, MD
        Point { latitude: 42.358894, longitude: -71.056742 }, // Boston, MA
        Point { latitude: 42.733635, longitude: -84.555328 }, // Lansing, MI
        Point { latitude: 44.955097, longitude: -93.102211 }, // Saint Paul, MN
        Point { latitude: 32.303848, longitude: -90.182106 }, // Jackson, MS
        Point { latitude: 38.579201, longitude: -92.172935 }, // Jefferson City, MO
        Point { latitude: 46.585709, longitude: -112.018417 }, // Helena, MT
        Point { latitude: 40.808075, longitude: -96.699654 }, // Lincoln, NE
        Point { latitude: 39.163914, longitude: -119.766121 }, // Carson City, NV
        Point { latitude: 43.206898, longitude: -71.537994 }, // Concord, NH
        Point { latitude: 40.220596, longitude: -74.769913 }, // Trenton, NJ
        Point { latitude: 35.68224, longitude: -105.939728 }, // Santa Fe, NM
        Point { latitude: 35.78043, longitude: -78.639099 }, // Raleigh, NC
        Point { latitude: 46.82085, longitude: -100.783318 }, // Bismarck, ND
        Point { latitude: 39.961346, longitude: -82.999069 }, // Columbus, OH
        Point { latitude: 35.492207, longitude: -97.503342 }, // Oklahoma City, OK
        Point { latitude: 44.938461, longitude: -123.030403 }, // Salem, OR
        Point { latitude: 40.264378, longitude: -76.883598 }, // Harrisburg, PA
        Point { latitude: 41.830914, longitude: -71.414963 }, // Providence, RI
        Point { latitude: 34.000343, longitude: -81.033211 }, // Columbia, SC
        Point { latitude: 44.367031, longitude: -100.346405 }, // Pierre, SD
        Point { latitude: 36.16581, longitude: -86.784241 }, // Nashville, TN
        Point { latitude: 30.27467, longitude: -97.740349 }, // Austin, TX
        Point { latitude: 40.777477, longitude: -111.888237 }, // Salt Lake City, UT
        Point { latitude: 44.262436, longitude: -72.580536 }, // Montpelier, VT
        Point { latitude: 37.538857, longitude: -77.43364 }, // Richmond, VA
        Point { latitude: 47.035805, longitude: -122.905014 }, // Olympia, WA
        Point { latitude: 38.336246, longitude: -81.612328 }, // Charleston, WV
        Point { latitude: 43.074684, longitude: -89.384445 }, // Madison, WI
        Point { latitude: 41.140259, longitude: -104.820236 }, // Cheyenne, WY
    ];

    let k = 5; // Number of clusters

    let clusters = lloyds_algorithm(&state_capitals, k);

    for (i, cluster) in clusters.iter().enumerate() {
        println!("Cluster {}:", i + 1);
        println!("  Center: ({}, {})", cluster.center.latitude, cluster.center.longitude);
        println!("  Points:");
        for point in &cluster.points {
            println!("    ({}, {})", point.latitude, point.longitude);
        }
    }
}
