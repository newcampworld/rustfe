use std::io;

fn main() {
    println!("Enter coordinates as 'x y' (e.g., '3 4'). Type 'q' to quit.");

    loop {
        let mut line = String::new();
        // Read a line from stdin
        match io::stdin().read_line(&mut line) { // an input is received & assessed, then next
            /*
            The string variable `line` is initially empty, so we need to modify it.
            It's passed as a mutable reference (&mut) because the `read_line` function
            borrows and appends the user's input (including the newline character) to it.
            Input is captured when the user presses "Enter."
            */
            Ok(_) => {
                // Ok indicates that the operation was successful. It contains the number of characters read, 
                // but here we’re ignoring it (_) because we don’t need that value.
                let trimmed = line.trim(); // Remove newline and spaces

                // Check for termination keyword
                if trimmed.eq_ignore_ascii_case("q") {
                    println!("Exiting the program.");
                    break;
                }

                // Split the input into parts
                let parts: Vec<&str> = trimmed.split_whitespace().collect();
                /*
                If the input "4 9" is received and processed using the code:
                the vector parts will be: vec!["4", "9"]
                The vector Vec<&str> itself is stored on the heap.
                Each element of the vector is a string slice (&str) that points to parts 
                of the original String (in this case, the string line in memory).
                These slices do not allocate additional memory for the string 
                content—they just reference the segments of the original string.

                When split_whitespace() is called on the trimmed string, 
                it creates an iterator that lazily produces each substring separated by whitespace.
                The collect() method takes all the items produced by the iterator and gathers 
                them into a single collection—in this case, a Vec<&str>.
                */

                // Ensure we have exactly two parts
                if parts.len() != 2 {
                    println!("Please enter exactly two integers, separated by a space.");
                    continue;
                }

                // Parse the input into integers
                let x: i32 = match parts[0].parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Invalid input for x. Please enter integers only.");
                        continue;
                    }
                };

                let y: i32 = match parts[1].parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Invalid input for y. Please enter integers only.");
                        continue;
                    }
                };

                // Create a tuple and match
                let point = (x, y);
                match point {
                    (0, 0) => println!("Origin"),
                    (x, 0) => println!("On the x-axis at {}", x),
                    (0, y) => println!("On the y-axis at {}", y),
                    (x, y) => println!("At coordinates ({}, {})", x, y),
                }
            }
            Err(err) => {
                eprintln!("Error reading input: {}", err);
                /*
                This macro is similar to println!, but it writes to the standard error stream (stderr) 
                instead of the standard output stream (stdout).
                This is especially useful in programs where the distinction between standard output 
                and errors is important (e.g., when redirecting output to a file).
                */
                continue;
                /*
                When Does Err(err) Happen?
                    The Err(err) case happens when a function returning a Result encounters an issue.
                    For std::io::stdin().read_line(), this could include:
                        Input/output errors, such as:
                        The program cannot read from standard input (e.g., due to a closed or broken input stream).
                        Low-level system errors, like a lack of resources or a hardware failure.
                    These scenarios are rare under normal usage but must be handled to ensure robust code.
                */
            }
        }
    }
}
