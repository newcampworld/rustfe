fn main(){

/*
static variables are allocated in the data segment of the program's memory, 
specifically in the read-only data segment for immutable values or the writable data segment for mutable values
Unlike const, static variables always have a fixed memory address location, which can be referenced at runtime
Scope: The value of static is global and persists for the lifetime of the program. It is accessible from any part of the code.
*/
static GLOBAL_VAR: i32 = 42; // Initialized data
static mut UNINITIALIZED_VAR: i32 = 0; // BSS segment (uninitialized data)
let heap_var = Box::new(0;10);
/*
`Box` is a smart pointer that provides ownership of heap-allocated memory.
When calling `Box::new([0u8; SIZE])`:
    a. Memory for the array `[0u8; SIZE]` is allocated on the heap.
    b. The array is initialized (all elements set to `0u8`).
    c. A pointer to this heap-allocated memory is returned, encapsulated in a `Box`.
*/
let stack_var = 5; // Local variable on the stack
const SIZE: usize = 100_000; // Size of the heap-allocated array
/*
Constants are not allocated in memory (stack or heap) (No memory address). Instead, the compiler embeds 
the value directly into the machine code at every point where the constant is referenced.
Scope: The value of const is available throughout its declared scope. It behaves similarly to an inline value.
*/
const N_ARRAY: usize = 1_000_000; // Number of recursive calls
fn create_array() -> Box<[u8; SIZE]> { 
    Box::new([0u8; SIZE]) // are elements are initialized to 0
    // allocates memory on the heap large enough to hold the array then 
    // return a Box<T>, which contains a pointer to the heap-allocated memory
}
/*
Allocation Steps for `Box<T>`:
    1. The stack allocates space for a pointer.
    2. The heap allocates space for the object being referenced, which an array of size 100K element
    3. The pointer on the stack points to the heap-allocated memory. 
*/
fn recursive_func(n: usize) {
    let a = create_array();
    // a is of type Box<[u8; SIZE]>. This means a is a smart pointer that owns the memory allocated on the heap for the array [0u8; SIZE]
    println!("{} {}", N_ARRAY - n + 1, a[0]); // prints the recursive call number and the first element of the array.
    if n > 1 { 
        recursive_func(n - 1) // makes another recursive call if `n > 1`.
    }
}
recursive_func(N_ARRAY);
}

/*
Memory usage here is as follows:
    The array size per recursion is: 100,000 × 1 byte (u8 is one byte) = 100,000 bytes ≈ 100 KB
    For 1,000,000 recursions, the total heap allocation is ≈ 1,000,000 x 100 KB ≈ 100 GB
This would likely exhaust available memory, causing the program to crash with a memory allocation error.
*/ 
    
/*
Each process has its own memory segments, which are divided into the following key sections:
(Threads of a single process: Share text, data, and heap segments but maintain their own stack.)

+---------------------------+ High Memory Address
|                           |
|        Stack              | Used for function call stack and local variables
|                           | (grows downward).
+---------------------------+
|                           |
| Memory-mapped Segment     | Used for shared libraries and memory-mapped files.
|                           |
+---------------------------+
|                           |
|        Heap               | Used for dynamically allocated memory 
|                           | (grows upward during runtime).
+---------------------------+
|                           |
|   Read-Only Segment       | Contains immutable global/static variables
|                           | and constants (shared across threads).
+---------------------------+
|                           |
|       BSS Segment         | Contains uninitialized global/static variables 
|                           | (set to zero by default).
+---------------------------+
|                           |
|      Data Segment         | Stores global and static variables.
|                           |
+---------------------------+
|                           |
|      Text Segment         | Contains the program's executable code.
|                           | Usually read-only.
+---------------------------+ Low Memory Address
*/

