// Fallibale Type Converter
// Since there's a possibility of failure, the result of such a conversion is typically wrapped in a Result type

use std::convert::TryFrom;

// Define a new type that wraps `i64`
struct MyInt64(i64);

// Implement `TryFrom<MyInt64>` for `i32`
impl TryFrom<MyInt64> for i32 {
    type Error = &'static str;

    fn try_from(value: MyInt64) -> Result<Self, Self::Error> {
        let value = value.0;
        if value >= i32::MIN as i64 && value <= i32::MAX as i64 {
            Ok(value as i32)
        } else {
            Err("Value out of range for i32")
        }
    }
}

fn main() {
    let large_number = MyInt64(1234567890123);
    let small_number = MyInt64(12345);

    match i32::try_from(large_number) {
        Ok(val) => println!("Converted large_number to i32: {}", val),
        Err(e) => println!("Failed to convert large_number: {}", e),
    }

    match i32::try_from(small_number) {
        Ok(val) => println!("Converted small_number to i32: {}", val),
        Err(e) => println!("Failed to convert small_number: {}", e),
    }
}
