use rand::Rng;
/*
Supervised Learning Algorithm
    Nature: This is the training process. It's an algorithm that:
        Takes the training data as input.
        Generates a hypothesis or predictive model by learning patterns in the data.
*/
fn train_model(data: Vec<(Vec<f64>, usize)>) -> PredictiveModel {
    let mut weights = vec![0.0; data[0].0.len()]; // Initialize weights to zero
    // data[0].0.len() only operates on the first tuple in the data vector and 
    // retrieves the length of the first inner vector in that tuple.
    let learning_rate = 0.1; // The learning rate (η=0.1) ensures the updates are gradual. A larger ηη would result in faster but less stable updates

    // Simulate a simple gradient descent-like process
    for _ in 0..100 { // Iterating 100 times over the data
        for (features, label) in &data {
            let predicted: f64 = weights.iter().zip(features.iter())
                                        .map(|(w, x)| w * x)
                                        .sum();
            // y^ ​= w1​⋅x1 ​+ w2​⋅x2 ​+ ⋯ + wn​⋅xn
            // The error is the difference between the actual label (y) and the predicted value (y^​): error=y−y^
            let error = *label as f64 - predicted;
            for i in 0..weights.len() {
                weights[i] += learning_rate * error * features[i]; // Adjust weights
                // current weight + Learning rate * error * The feature value corresponding to the weight
            }
        }
    }

    PredictiveModel { weights } //  'PredictiveModel { weights: vec![−0.46,1.24,0.03]}'
    /*
    Inside the PredictiveModel { weights } vector, the weights are the parameters of the 
    predictive model that are being iteratively updated during training to "learn" the 
    relationship between inputs (features) and outputs (labels). These weights determine 
    how much influence each feature has on the final prediction.
    */
}

/*
Predictive Model (Hypothesis)
    Nature: The predictive model is the result of training the supervised learning algorithm on the input-output pairs.
    It doesn’t “store” the output from the training data. Instead, it learns a pattern or function to predict outputs for new data
    It which can be:
        A set of parameters: For example, in linear regression, the model is just a set of learned weights.
        A decision function: For example, in decision trees, the model is a tree-like structure that makes decisions.
        A trained system: In neural networks, it’s a set of interconnected weights.
    Moreover, the predictive model is essentially:
        A Data Structure or Object: It stores the learned parameters (e.g., weights for linear regression).
        A Predict Function: A callable function or method that takes new input data and applies the learned parameters to predict the output.
*/
struct PredictiveModel {
    weights: Vec<f64>, // Weights = [−0.46,1.24,0.03]
    /*
    Weight for Feature 1 (-0.4602):
        This feature has a negative influence on the prediction.
        Higher values of this feature decrease the likelihood of the traffic being classified as malicious.

    Weight for Feature 2 (1.2404):
        This feature has the strongest positive influence on the prediction.
        Higher values significantly increase the likelihood of the traffic being classified as malicious.

    Weight for Feature 3 (0.0345):
        This feature has a minor positive influence on the prediction.
        Its impact is minimal compared to the other features.
    */
}

impl PredictiveModel {
    // Method to predict if the traffic is malicious or benign
    fn predict(&self, input: &Vec<f64>) -> usize {
        let score: f64 = self.weights.iter().zip(input.iter())
                                     .map(|(w, x)| w * x)
                                     .sum();
        // The predictive model compares each new traffic vector to the learned weights
        if score > 0.5 { 1 } else { 0 } // 1 = Malicious, 0 = Benign
        /*
        Example 1:
            Features: [1.0,0.5,0.2][1.0,0.5,0.2], Weights: [−0.4602,1.2404,0.0345][−0.4602,1.2404,0.0345]
            score=(−0.4602⋅1.0)+(1.2404⋅0.5)+(0.0345⋅0.2)
            score=−0.4602+0.6202+0.0069=0.1669
            so it is classified as Benign
        
        Example 2:
            Features: [2.0,1.5,0.3][2.0,1.5,0.3], Weights: [−0.4602,1.2404,0.0345][−0.4602,1.2404,0.0345]
            score=(−0.4602⋅2.0)+(1.2404⋅1.5)+(0.0345⋅0.3)
            score=−0.9204+1.8606+0.0104=0.9505
            so it is classified as Malicious
        */
    }
}

fn main() {
    // 10 labeled samples of network traffic (inputs and labels)
    let training_data = vec![
        (vec![1.0, 0.5, 0.2], 0),  // Benign
        (vec![2.0, 1.5, 0.3], 1),  // Malicious
        (vec![0.8, 0.4, 0.1], 0),  // Benign
        (vec![1.5, 1.0, 0.5], 1),  // Malicious
        (vec![1.0, 0.6, 0.3], 0),  // Benign
        (vec![1.8, 1.2, 0.7], 1),  // Malicious
        (vec![0.7, 0.3, 0.2], 0),  // Benign
        (vec![2.2, 1.7, 0.8], 1),  // Malicious
        (vec![1.1, 0.7, 0.4], 0),  // Benign
        // Each sample has 3 features (e.g., traffic size, packet type ratio, unusual behavior flag) 
        // and a label (1 = malicious, 0 = benign).
        (vec![1.9, 1.4, 0.6], 1),  // Malicious
    ];
    /*
    Labeled Training Data: Input-output pairs provided to the learning algorithm.
    Nature: This is just structured data. It typically consists of:
        Inputs: Features (e.g., numerical data, images, or text).
        Labels: Outputs (e.g., categories like "cat" or "dog," or numerical values like housing prices).
    
    Moreover, the output is the label or target value that corresponds to the input data. 
    It's the desired result we want the algorithm to predict. Examples of Inputs and Outputs:
    1. Classification (Labels as Outputs)
        Input: Features of an object.
        Output: A label indicating the category the object belongs to.
        Example:
            Input: Features of an email (e.g., words used, sender info).
            Output: Label = "Spam" or "Not Spam".

    2. Regression (Numeric Outputs)
    Input: Features of a scenario.
    Output: A numeric value representing a continuous variable.
        Example:
            Input: Features of a house (e.g., size, location, number of rooms).
            Output: Predicted price = $300,000.
    4. Image Classification
        Input: Pixels of an image.
        Output: A label identifying what the image contains.
        Example:
            Input: Image data of a cat (e.g., pixel intensities).
            Output: Label = "Cat".
    */

    // Training Phase:
        // Use input-output pairs (labeled data) to train the supervised learning algorithm.
        // The algorithm produces the predictive model.
    let model = train_model(training_data);

    // Our real data. Generate 100 new samples of network traffic with random features
    let mut rng = rand::thread_rng();
    let mut new_traffic_data = Vec::new();
    for _ in 0..100 {
        new_traffic_data.push(vec![
            rng.gen_range(0.0..3.0), // Random feature 1, traffic size
            rng.gen_range(0.0..2.0), // Random feature 2, packet type ratio
            rng.gen_range(0.0..1.0), // Random feature 3, unusual behavior flag
        ]);
    }

    let mut malicious_count = 0;
    let mut benign_count = 0;
    // Prediction Phase:
        // The predictive model takes new inputs and generates outputs (predictions).
    for input in &new_traffic_data {
        let prediction = model.predict(input);
        if prediction == 1 {
            malicious_count += 1;
        } else {
            benign_count += 1;
        }
    }

    // Output the results
    println!("Out of 100 traffic samples:");
    println!("Malicious: {}", malicious_count);
    println!("Benign: {}", benign_count);
}

/*
Machine learning in supervised learning involves using input-output pairs (labeled data)
to train an algorithm that learns the relationship between the inputs and the outputs. 
The input consists of features (e.g., numerical values, images, or text), and the output
is the known result or label (e.g., a category like "cat" or a numeric value like "price").
During the training phase, a supervised learning algorithm processes this data to build
a predictive model, which is essentially a representation of the learned patterns or a
function that maps inputs to outputs. The output labels in the training data are 
critical for guiding the algorithm to minimize errors and improve predictions.

The predictive model is best understood as an object that encapsulates the learned parameters
(properties) and optionally provides methods (functions) for making predictions. These methods
might include a predict function, which processes new inputs to generate outputs, or external
functions can use the model’s parameters to perform predictions. In its essence, the 
predictive model is an object with data (parameters) and optional methods, designed 
to generalize the patterns learned during training and predict outcomes for unseen data.
It is not simply a mathematical function but rather an encapsulation 
of learned behavior, ready for deployment in real-world tasks.
*/

/*
Gradient descent is a foundational algorithm in optimization and machine learning, originating
from calculus and optimization theory. The formula is based on the principle of moving iteratively
in the direction of the negative gradient of a loss function to minimize it. This idea was first
formalized by Augustin-Louis Cauchy in 1847 through the method of steepest descent. It was later
adapted for machine learning by researchers like Léon Bottou, who introduced stochastic gradient
descent (SGD), making the algorithm efficient for handling large datasets and iterative updates.

The algorithm is widely used in areas beyond machine learning, including deep learning (to train neural networks),
numerical optimization (in physics simulations and engineering design), and robotics (for policy learning
in reinforcement learning). In supervised learning, gradient descent fits naturally because training a
model is an optimization problem: minimizing a loss function that measures the difference between predicted
and actual values. This iterative adjustment of model parameters, guided by the gradient, allows models to
learn patterns effectively, even in high-dimensional data spaces.
*/