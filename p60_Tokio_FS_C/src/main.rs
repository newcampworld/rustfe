use futures::future::join_all;
use tokio::fs::File as AsyncFile;
use tokio::io::AsyncWriteExt;
use std::io;
use std::future::Future;
use std::pin::Pin;

async fn run_async() -> io::Result<()> {
    let tasks: Vec<Pin<Box<dyn Future<Output = io::Result<()>>>>> = vec![
        Box::pin(async {
            let mut t1 = AsyncFile::create("task1.txt").await?;
            t1.write_all("task1".as_bytes()).await?;
            println!("Finished writing task1");
            Ok::<(), io::Error>(())
        }),
        Box::pin(async {
            let mut t2 = AsyncFile::create("task2.txt").await?;
            t2.write_all("task2".as_bytes()).await?;
            println!("Finished writing task2");
            Ok::<(), io::Error>(())
        }),
        Box::pin(async {
            let mut t3 = AsyncFile::create("task3.txt").await?;
            t3.write_all("task3".as_bytes()).await?;
            println!("Finished writing task3");
            Ok::<(), io::Error>(())
        }),
        Box::pin(async {
            let mut t4 = AsyncFile::create("task4.txt").await?;
            t4.write_all("task4".as_bytes()).await?;
            println!("Finished writing task4");
            Ok::<(), io::Error>(())
        }),
    ];

    join_all(tasks).await.into_iter().collect::<Result<Vec<_>, _>>()?;

    Ok(())
}

#[tokio::main]
async fn main() -> io::Result<()> {
    run_async().await?;
    println!("All tasks completed.");
    Ok(())
}

/* 
First, since Rust does not allow multiple different async {} blocks inside a Vec, we can convert each future 
into a Box<dyn Future<Output = io::Result<()>>> to give them a common type:

Second, each element inside the vec![] is an async block. The async keyword is necessary because:

    It creates a future that can be executed asynchronously.
    AsyncFile::create(...).await and t1.write_all(...).await are async operations, 
    and Rust does not allow .await outside of an async context.
    The function join_all(tasks).await expects a collection of futures, and async { ... } is 
    the easiest way to create a future inline. 
    
Another way to define each task is to use a normal async fn instead of inline async {} blocks: 

async fn write_task(filename: &str, content: &str) -> io::Result<()> {
    let mut file = AsyncFile::create(filename).await?;
    file.write_all(content.as_bytes()).await?;
    println!("Finished writing {}", filename);
    Ok(())
}

async fn run_async() -> io::Result<()> {
    let tasks = vec![
        write_task("task1.txt", "task1"),
        write_task("task2.txt", "task2"),
        write_task("task3.txt", "task3"),
        write_task("task4.txt", "task4"),
    ];
*/