/*
Abstract Explanation
    Goal: The perceptron tries to draw a linear boundary that separates two classes
    Inputs: A set of features, x1, x2, ..., xm
    Weights & Bias: Each feature 𝑥 has an associated weight w, plus a bias b
    Net Input: The perceptron computes a weighted sum: z = x1w1 + x2w2 + ... + b
    Threshold / Activation: If z≥0, predict class 1; else predict class 0.
    Learning: Whenever we misclassify a training example, we adjust (nudge) the weights and bias 
    so that next time, we’re more likely to classify it correctly.
    Key takeaway: Think of the perceptron as a straight line (in 2D) or hyperplane 
    (in higher dimensions) that it tries to move around (by adjusting weights & bias) 
    until it can separate the data as best as it can.
*/

use rand::Rng;

// Simple Perceptron structure.
struct Perceptron {
    weights: Vec<f64>,
    bias: f64,
    learning_rate: f64,
}

impl Perceptron {
    // Create a new Perceptron with 'num_features' weights,
    // each initialized to a small random value.
    fn new(num_features: usize, learning_rate: f64) -> Self {
        let mut rng = rand::thread_rng();
        let weights = (0..num_features)
            .map(|_| rng.gen_range(-0.01..0.01)) // small random weights. A vector with multiple near zero elements is initialized
            .collect();
        
        let bias = rng.gen_range(-0.01..0.01);   // small random bias, near zero
        
        Perceptron {
            weights,
            bias,
            learning_rate,
        }
    }

    // Predict output (0 or 1) using current weights & bias.
    fn predict(&self, inputs: &[f64]) -> i32 {
        // 1) Calculate net input z = (w.x + b)
        let mut sum = self.bias;
        for (w, x) in self.weights.iter().zip(inputs.iter()) {
            sum += w * x; // Net Input
            /*
            As an example, if weights = [0.5, -0.3] and inputs = [1.0, 0.2], then zip produces [(0.5, 1.0), (-0.3, 0.2)]
            Thus, given the weighted sum formula sum = ∑ wj.xj + b, sum+=w⋅x=0.5⋅1.0=0.5, then sum+=w⋅x=−0.3⋅0.2=−0.06
            and given b, say 1, z becomes 0.54
            */
        }

        // 2) Threshold function: if sum >= 0 => 1 else => 0
        if sum >= 0.0 { 1 } else { 0 }
    }

    // Train the perceptron with given training data for a specified number of epochs
    fn train(&mut self, training_data: &[(Vec<f64>, i32)], epochs: usize) {
        for _epoch in 0..epochs {
            // We loop over all training examples multiple times (each full pass is an “epoch”) 
            // until the perceptron converges or we reach a maximum epoch limit.
            for (inputs, label) in training_data { // tuple entry for each loop is a two-elemented vector and an integer 
                // The perceptron processes one training example (inputs) at a time, 
                // computes the prediction, and updates weights if necessary
                let prediction = self.predict(inputs);
                let error = label - prediction; 
                /*
                If error is zero, no update. If not, update weights & bias.
                If the example was supposed to be class 1 but the perceptron predicted 0, it increases 𝑤𝑗 (and 𝑏)
                so that next time, the net input 𝑧 will be higher for a similar 𝑥^i.
                If the example was supposed to be 0 but predicted 1, it decreases 𝑤𝑗 (and 𝑏),
                lowering the net input next time.

                When it misclassifies, it adjusts (pushes/pulls) the weights and bias by 
                an amount proportional to the error and the input values.
                */
                if error != 0 {
                    /*
                    When does it converge?
                    It converges if the data is linearly separable (meaning, there really is a clean straight line boundary 
                    that can separate class 0 from class 1).
                    If data is not linearly separable, it might not converge, and you’ll either keep updating forever 
                    or stop early after a certain number of passes.
                    */

                    // Update each weight
                    for (j, w) in self.weights.iter_mut().enumerate() {
                        *w += self.learning_rate * (error as f64) * inputs[j];
                    }
                    // Update bias
                    self.bias += self.learning_rate * (error as f64);
                }
            }
        }
    }
}

fn main() {
    // Example: We'll train it on a simple OR dataset 
    // x1, x2 => label
    let training_data = vec![
        (vec![0.0, 0.0], 0),
        (vec![0.0, 1.0], 1),
        (vec![1.0, 0.0], 1),
        (vec![1.0, 1.0], 1),
    ];
    
    // Create a Perceptron with 2 features & a learning rate of 0.1
    let mut perceptron = Perceptron::new(2, 0.1);
    
    // Train for 10 epochs
    perceptron.train(&training_data, 10);

    // Test predictions
    println!("Predict(0,0) = {}", perceptron.predict(&[0.0, 0.0]));
    println!("Predict(0,1) = {}", perceptron.predict(&[0.0, 1.0]));
    println!("Predict(1,0) = {}", perceptron.predict(&[1.0, 0.0]));
    println!("Predict(1,1) = {}", perceptron.predict(&[1.0, 1.0]));
}
