use std::net::UdpSocket;

fn main() -> std::io::Result<()> {
    // Create a UDP socket bound to any available port
    let socket = UdpSocket::bind("0.0.0.0:0")?;

    // Send a message to the server at 127.0.0.1:8080
    /*
    In Rust, all data sent over sockets (including UDP) must be in the form of a byte slice (&[u8]). 
    When you send a string, it is converted to bytes using UTF-8 encoding; with "b" at the beginning of the string
    You can reate a byte slice from a string literal by prefixing it with b. This tells the Rust compiler to interpret the string as a sequence of bytes.
    */
    let message = b"Hello from the client!";
    /*
    This creates a byte slice &[u8]. An array of bytes representing the string in UTF-8 format.
    The message is only 20 bytes (18 bytes of text + 2 bytes for UTF-8 encoding overhead. 
    If the message were larger than 1024 bytes, the server would only receive the first 1024 bytes, 
    and the rest would be discarded (UDP doesn't handle fragmentation automatically).
    */
    socket.send_to(message, "127.0.0.1:8080")?;

    /*
    If we were to send raw binary data, we can use a byte array like this:
    let binary_data: [u8; 4] = [0x12, 0x34, 0x56, 0x78];
    Each hexadecimal digit represents 4 bits (half a byte), so two hex digits represent a full byte (8 bits). 
    For example, 0x12 is one byte, where 1 represents the first 4 bits (0001 in binary) and 2 represents the next 4 bits (0010 in binary), 
    resulting in the byte 00010010. Remember, that hex is a compact way to represent byte values.
    On the server side, you can receive the raw binary data and handle it as follows:
    let data = &buf[..amt]; // Slice the buffer to only include the received data
    println!("Received {:?} from {}", data, src);
    Here, the data is printed directly as raw bytes (without any conversion) using the {:?} formatter, 
    which outputs the byte array in hexadecimal format.
    */

    // Create a buffer to receive the response
    let mut buf = [0; 1024];
    let (amt, _src) = socket.recv_from(&mut buf)?;

    // Print the server's response
    println!("Received from server: {}", String::from_utf8_lossy(&buf[..amt]));

    Ok(())
}
