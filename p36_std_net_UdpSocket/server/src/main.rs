use std::net::UdpSocket;

fn main() -> std::io::Result<()> {
    /*
    std::io::Result<T> is just an alias for Result<T, std::io::Error>.
    If the operation succeeds, it will return Ok(()), If it fails, it will return Err(std::io::Error)
    std::io::Result<()> is used when the function doesn't return any meaningful value on success, but it can still return an error
    */
    
    /*
    Bind the UDP socket to an address (localhost:8080)
    Since there’s no need for a handshake or to accept connections, once the socket is bound, 
    it’s ready to send and receive packets immediately using send_to and recv_from without an explicit listen function.
    */
    let socket = UdpSocket::bind("127.0.0.1:8080")?;
    /*
    This function returns a Result<UdpSocket, std::io::Error>.
    The ? operator checks the Result:
        If it’s Ok(socket), the socket is assigned to the socket variable.
        If it’s Err(e), the function returns the error e immediately, and the calling code can handle it.
    */

    /*
    The ? operator in Rust is a shorthand for error handling. It is used to propagate errors without explicitly writing match statements. 
    Whenever you see ? after a function call, it means:
        If the function call succeeds: It extracts the successful result and lets the program continue.
        If the function call returns an error: It immediately returns the error from the current function to the caller.
    Without the ? operator, you would need to write something like this:
    let socket = match UdpSocket::bind("127.0.0.1:8080") {
        Ok(s) => s,              // If successful, store the socket. Unwraps the Result and continues with the successful value.
        Err(e) => return Err(e), // If failed, return the error. Propagates the error and immediately returns it from the current function.
    };
    */

    println!("Server is running on 127.0.0.1:8080");

    // Create a buffer to store received data
    let mut buf = [0; 1024]; // buffer size of 1024 bytes; each element initialized with zero

    /*
    This is a safe size for most small UDP messages. It means the server can receive up to 1024 bytes in one packet.
    */

    loop { // infinite loop
        // Wait for a message (blocking call)
        let (amt, src) = socket.recv_from(&mut buf)?;
        // amt, amount of received bytedata stored in buffer
        // src, address of the sender

        /*
        It returns a Result<(usize, SocketAddr), std::io::Error>.
        If it's Ok((amt, src)), it continues and assigns the result: amt is the number of bytes received, and src is the source address of the sender.
        If it’s Err(e), the ? operator propagates the error up the call stack.
        */

        // Print received message and source address
        println!(
            "Received {} bytes from {}: {}",
            amt,
            src,
            String::from_utf8_lossy(&buf[..amt])
            /*
            The slice &buf[..amt] contains only the valid data received, preventing any attempts to read beyond the number of bytes that were actually received.
            String::from_utf8_lossy(...) converts the byte slice back into a string for easier reading. 
            If the byte data isn't valid UTF-8, it replaces invalid sequences with the replacement character (�).
            */
        );

        // Echo the message back to the client
        socket.send_to(&buf[..amt], src)?;
    }
}

/*
Byte Slice

let array1 = [1, 4, 7, 88, 6, 9, 13, 4, 7, 5, 9, 55] // we can say this is an integer array
let slice1 = &a[..6];

let byte_array: [u8; 6] = [72, 101, 108, 108, 111, 33]; // Represents "Hello!". Capacity of each room is not more than 255
    
    // Creating a byte slice from the byte array
    let byte_slice: &[u8] = &byte_array[1..5]; // This slice contains [101, 108, 108] ("ell")
    // Just like &array1[..6] gives you access to the first 6 elements of the integer array, 
    // a byte slice gives you access to a contiguous range of bytes in a byte array.
    
    // Printing the byte slice
    println!("{:?}", byte_slice); // Output: [101, 108, 108]

    // Converting byte slice to a string
    let string = std::str::from_utf8(byte_slice).unwrap(); // Convert byte slice to &str
    println!("{}", string); // Output: ell

In Rust, bytes (u8) are often used to represent data at a low level (like text, images, etc.). 
When dealing with networking or file operations, data is typically handled as a series of bytes, which is where byte slices come into play.

Slice: A slice (&[T] where T is any type) is a reference to a contiguous segment of an array or vector.
Byte Slice: &[u8] is specifically a reference to a contiguous segment of bytes, commonly used for raw binary data.
byte arrays ([u8; N]) are used for *efficient* storage of binary data.
*/
