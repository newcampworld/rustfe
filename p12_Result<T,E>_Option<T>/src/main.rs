/*
In Rust, the safest and most idiomatic way to handle function calls that might fail is to 
design your callee function to return a Result<T, E> or Option<T>: 

        Option<T>
        The Option<T> enum is used when a value may or may not be present. 
        It allows you to explicitly handle cases where a variable might be undefined or absent.
        enum Option<T> {
            Some(T), // Contains a value of type T
            None,    // Represents no value
        }


        Result<T, E>
        The Result<T, E> enum is used when a function might succeed or fail. 
        It allows you to handle success (Ok) or failure (Err) explicitly.
        enum Result<T, E> {
            Ok(T),   // Contains the result value of type T
            Err(E),  // Contains an error value of type E
        }

and then use appropriate error-handling techniques in the caller. 
This approach enforces explicit handling of errors and missing values, ensuring robust and maintainable code.

    - Callee: The callee should return a Result<T, E> or Option<T> based on the situation.
    - Caller: Handle the Return Value in the Caller:
        Use match for explicit handling.
        Use combinators like map, and_then, or unwrap_or for concise logic.
        Use the ? operator for propagation when appropriate.
*/


use std::fs::File;
use std::io::{self, Read};

fn find_item(index: usize, items: &[i32]) -> Option<i32> {
    items.get(index).cloned() // Returns Some(item) or None
}

fn read_file_to_string(file_path: &str) -> Result<String, io::Error> {
    let mut file = File::open(file_path)?; // ? propagates the error if present
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

fn main() {

    // enum Result
    match read_file_to_string("example.txt") {
        Ok(data) => println!("File contents: {}", data),
        // data will hold the contents of the file example.txt if the file is successfully read
        Err(e) => eprintln!("Error reading file: {}", e),
    }

    // enum Option
    let numbers = vec![10, 20, 30];
    match find_item(1, &numbers) {
        Some(value) => println!("Found: {}", value),
        None => println!("Item not found!"),
    }
}