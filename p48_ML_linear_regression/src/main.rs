
/*
When we have exactly two points, determining the equation of the line is straightforward. 
A unique straight line passes through the two points, and its slope describes how steeply 
the line rises or falls. Using the slope and one of the points, you can construct the 
equation of the line directly. This approach is simple because no approximation is required; 
the line perfectly connects the two points.

With more than two points, it’s often impossible for a single line to pass through all the points. 
Instead, we use linear regression to find the line that best fits the data. This line 
minimizes the overall error, specifically the squared differences between the observed points 
and the predicted points on the line. The result is a line that represents the general trend of the data.
The least squares method is the technique used in linear regression to calculate the best-fit line. 
It works by minimizing the sum of the squared vertical distances (errors) between the observed 
points and the line; because, Linear Regression is about finding a line 
(or hyperplane in multiple dimensions) that best fits your data by minimizing the sum of squared errors. 
This ensures that the line is as close as possible to all the points 
collectively, making it an effective way to summarize the relationship between two variables in a dataset.
*/
use rand::Rng;
use std::f64;

fn linear_regression(x_vals: &[f64], y_vals: &[f64]) -> (f64, f64) {
    assert_eq!(x_vals.len(), y_vals.len(), "Input slices must have the same length");

    let n = x_vals.len() as f64;

    // Calculate means
    let mean_x: f64 = x_vals.iter().sum::<f64>() / n;
    let mean_y: f64 = y_vals.iter().sum::<f64>() / n;

    // Compute the slope (beta_1) and intercept (beta_0)
    let mut numerator = 0.0;
    let mut denominator = 0.0;

    for i in 0..x_vals.len() {
        let x_diff = x_vals[i] - mean_x;
        let y_diff = y_vals[i] - mean_y;
        numerator += x_diff * y_diff;
        denominator += x_diff * x_diff;
        /*
        This part accumulates the error terms and variance, effectively accounting for the errors in the 
        data and adjusting the slope accordingly. Penalization happens indirectly through these accumulated terms.
        Penalization is baked into the mathematics of the formula.
        */
    }

    let beta_1 = numerator / denominator;
    let beta_0 = mean_y - beta_1 * mean_x;

    (beta_0, beta_1)
}

fn main() {
    // Training dataset:
    let x_vals = vec![0.0, 5.0, 10.0, 15.0, 20.0, 30.0, 35.0, 40.0, 50.0]; // throughput
    let y_vals = vec![50.0, 60.0, 70.0, 90.0, 100.0, 110.0, 120.0, 130.0, 150.0]; // latency

    // Train the linear regression model
    /*
    During the training phase, the Least Squares Method (LSM) (or other loss functions) is used 
    to evaluate how well the model’s predictions match the actual data. If the predicted results 
    deviate significantly from the actual data, the LSM penalizes the model by increasing the loss value.
    The difference between the actual and predicted values (y i -y pred i) is the error. 
    Larger errors indicate that the prediction is far from the actual data.
    */
    let (beta_0, beta_1) = linear_regression(&x_vals, &y_vals);

    println!("Intercept (β₀): {:.2}", beta_0);
    println!("Slope (β₁): {:.2}", beta_1);

    // Generate 9 new random data points (x values) within the *range* of the training data
    let mut rng = rand::thread_rng();
    let mut new_data = vec![];
    for _ in 0..9 {
        let new_x = rng.gen_range(0.0..50.0); // Random x in the range of 0 to 50
        let predicted_y = beta_0 + beta_1 * new_x; // y = mx+c
        new_data.push((new_x, predicted_y));
    }

    // Print predictions for the new data
    println!("\nNew Data Predictions:");
    for (i, (new_x, predicted_y)) in new_data.iter().enumerate() {
        println!("With throughput {:.2}, Latency might be {:.2}", new_x, predicted_y);
    }
}

/*
Ordinary Least Squares (OLS) is a specific application of the Least Squares Method (LSM) to linear regression problems
Sum of Squared Errors (SSE) is the specific quantity being minimized in LSM
*/