use std::net::TcpListener;
use std::io::{Read, Write};

fn main() -> std::io::Result<()> {
    // Bind the server to an address and port
    let listener = TcpListener::bind("127.0.0.1:8080")?;
    /* 
    TcpListener::bind(address) does all these: --A--
        creates a socket, 
        binds it to the specified address, 
        configures the socket as a listening socket internally
    */
    println!("Server is running on {}", listener.local_addr()?);

    // Accept incoming connections
    for stream in listener.incoming() {
        // incoming () returns an iterator that yields incoming connections to the TcpListener.
        // and each connection is returned as a Result<TcpStream, std::io::Error>.
        match stream {
            
            Ok(mut stream) => {
                println!("New connection: {}", stream.peer_addr()?);

                // Buffer to store incoming data
                let mut buffer = [0; 512];

                // Read data from the client
                match stream.read(&mut buffer) {
                    Ok(bytes_read) => {
                        if bytes_read > 0 {
                            println!("Received: {}", String::from_utf8_lossy(&buffer[..bytes_read]));

                            // Echo the data back to the client
                            stream.write_all(b"Message received!")?;
                        }
                    }
                    Err(e) => {
                        eprintln!("Failed to read from connection: {}", e);
                    }
                }
            }
            Err(e) => {
                eprintln!("Connection failed: {}", e);
            }
        }
    }

    Ok(())
}

/*
--A--:
In Python's socket library, after binding, we explicitly call listen() and then accept(). However, 
in Rust's TcpListener API, this distinction is implicit.
In Rust, the TcpListener combines the functionality of listening and accepting connections into a single API, 
abstracting away the explicit listen() step.
*/

/*
The TcpListener struct in Rust provides several methods to handle incoming connections and manage the listener:

    - incoming() for a simple loop-based server where blocking is acceptable. This internally calls accept() 
    for each incoming connection and returns an iterator over the accepted connections.
    - accept() for more control over connection handling.
    - set_nonblocking() for non-blocking or asynchronous servers.
    - try_clone() if you need to share the listener between threads.
*/