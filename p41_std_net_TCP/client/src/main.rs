/*
Core Components of std::net

    use std::net::TcpStream;
    use std::net::TcpListener;
    use std::net::UdpSocket;
    use std::net::SocketAddr;
    use std::net::IpAddr;
    use std::net::{Ipv4Addr, Ipv6Addr};
    use std::net::ToSocketAddrs;
*/

use std::net::TcpStream;
use std::io::{Write, Read};

fn main() -> std::io::Result<()> {
    let mut stream = TcpStream::connect("127.0.0.1:8080")?;
    let message = "Hello, server!";
    
    // First: Send a message to the server
    stream.write_all(message.as_bytes())?;
    println!("Sent: {}", message);

    // Second: Receive the response from the server
    let mut buffer = [0; 512];
    let bytes_read = stream.read(&mut buffer)?;
    println!("Response: {}", String::from_utf8_lossy(&buffer[..bytes_read]));

    Ok(())
}

