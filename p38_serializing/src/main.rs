use serde::{Serialize, Deserialize};
use std::fs::File;
use std::io::{self, Write};
use chrono::{Utc};

#[derive(Serialize, Deserialize, Debug)]
/*
The macro derive is used to automatically generate the implementation of a trait for a struct. 
In this case, for example, trait Serialize is implemented, impl, for your struct "Block".
*/
struct Block {
    id: u32,
    time: i64, 
    hash: String,
    previous_hash: String,
    data: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let blocks = vec![
        Block {
            id: 0,
            time: Utc::now().timestamp(),
            hash: String::from("hash1"),
            previous_hash: String::from("0"),
            data: String::from("Genesis Block"),
        },
        Block {
            id: 21,
            time: Utc::now().timestamp(),
            hash: String::from("hash2"),
            previous_hash: String::from("hash1"),
            data: String::from("Block 2 Data"),
        },
        Block {
            id: 2,
            time: Utc::now().timestamp(),
            hash: String::from("hash3"),
            previous_hash: String::from("hash2"),
            data: String::from("Block 3 Data"),
        },
    ];

    let json = serde_json::to_string(&blocks)?;
    /*
    If we had not used derive to automatically generate the implementation of the Serialize trait for the Block type, 
    any code that relies on serializing a Block into formats such as JSON would be impossible without manually implementing the Serialize trait.
    For example, the following expression would not work if Block doesn't implement the Serialize trait:

    let json = serde_json::to_string(&block)?;

    This expression tries to serialize a Block instance into a JSON string using the serde_json::to_string function. 
    Without derive(Serialize), Rust would produce a compile-time error because 
    the **to_string** function requires the Block type to implement the Serialize trait.

    */
    let mut file = File::create("blocks.json")?;
    file.write_all(json.as_bytes())?;
    
    Ok(())
}

/*
In Python, the dataclasses module provides a similar feature to Rust's derive for automatically 
generating methods like __init__(), __repr__(), and others. 

from dataclasses import dataclass

@dataclass
class Block:
    id: int
    time: int
    hash: str
    previous_hash: str
    data: str

# Usage
block = Block(1, 1620000000, "abcd1234", "xyz9876", "some data")
print(block)  # Automatically generated __repr__ method prints the block details
*/

/*
Why?
The serde_json::to_string function requires that its argument implements the Serialize trait so that it can convert 
the Rust data structure into a JSON string. Without this trait implementation, 
Rust has no way to understand how to transform the Block struct into JSON (or any other format).

What Would You Have to Do?
Without derive(Serialize), you would need to manually implement the Serialize trait for the Block struct:

use serde::ser::{Serialize, Serializer, SerializeStruct};

impl Serialize for Block {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("Block", 5)?;
        state.serialize_field("id", &self.id)?;
        state.serialize_field("time", &self.time)?;
        state.serialize_field("hash", &self.hash)?;
        state.serialize_field("previous_hash", &self.previous_hash)?;
        state.serialize_field("data", &self.data)?;
        state.end()
    }
}

Thus, without #[derive(Serialize)], any code that serializes Block (like converting it to JSON, YAML, etc.) 
would be impossible unless you provided a manual implementation of the Serialize trait.
*/