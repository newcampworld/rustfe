use std::mem::*;

fn main(){

    // &str static string
    let mut var1 = "hello"; // This is equivalent to: let mut var: &str = "hello";
    var1 = "world"; // This is allowed because `var` is mutable
    
    /*
    var.push('!'); This would result in a compile-time error because `&str` is immutable 
    Through 'var = "world"', we changed all the content of the string, not just some characters
    The line let mut var = "hello"; declares a mutable variable var that holds a 
    string slice (&str). However, it's important to note that even though the 
    variable var is mutable, the string slice itself is still immutable. 
    This means you can change what var points to, but you cannot modify 
    the string data "hello" through var. 
    It is not possible to overwrite some characters or to add or remove characters in such strings.
    */

    let a: &str = "";
    let b: &str = "0123456789";
    let c: &str = "abcdè"; // è is not in ASCI, so two bytes is dedicated for because of the UTF-8 notation
    println!("size of:\n a:{} b:{} c:{}",
    size_of_val(a),
    size_of_val(b),
    size_of_val(c));

    // str
    /*
    In Rust, you cannot declare a variable or a function argument of the type str. 
    This is because str represents a dynamically-sized type, which means its size 
    is not known at compile time. So, using this is wrong: std::mem::size_of::<str>(); 
    Each time the compiler parses a literal string, it stores in a static program area 
    the characters of that string, and that area is of str type.
    Instead, you must use a reference to a string slice (&str), which includes both 
    the pointer to the data and the length of the slice because it is a pair of a pointer and a length.
    let a: str;
    fn f(a: str) {}
    print!("{}", std::mem::size_of::<str>());
    */

    let a: &str = "";
    let b: &str = "0123456789";
    let c: &str = "abcdè";
    print!("{} {} {}; ",
    size_of_val(&a),
    size_of_val(&b),
    size_of_val(&c));
    print!("{} {} {}",
    size_of_val(&&a),
    size_of_val(&&b),
    size_of_val(&&c));

    // dynamic strings
    // The following way, we construct a dynamic string from a litral string which is static
    let mut a: String = "Xy".to_string(); // "Xy"
    a.remove(0); // "y"
    a.insert(0, 'H'); // "Hy"
    a.pop(); // "H"
    a.push('i'); // "Hi"
    print!("{}", a);

    // or the followings
    let s1 = String::new();
    let s2 = String::from("");
    let s3 = "".to_string();
    let s4 = "".to_owned();
    let s5 = format!("");
    print!("({}{}{}{}{})", s1, s2, s3, s4, s5);

    // Implementation
    /* Both Rust and C++ dynamic string types contain a dynamically allocated array of bytes, 
    which contains the characters of the string. Unlike string literals, String can be modified and grow in size.
    Both dynamic strings and vectors have the same implementation; they both use heap.
    */

    let s3 = "€".to_string();
    println!("S3 Capacity: {}", s3.capacity()); //allocates extra memory to optimize for future string modifications
    println!("S3 Length: {}", s3.len()); // requires 3 bytes in UTF-8 encoding

    let mut s1 = "".to_string();
    for _ in 0..16 {
        println!("{:p} {} {}",
        s1.as_ptr(), s1.capacity(), s1.len());
        // as_ptr() returns the address of the heap-allocated buffer containing the characters of the string
        s1.push('a'); // at each round one character is appended
        // it is quite clear that from 00000000 to 11111111, for instance, 8 bits is dedicated
    }
    let s2 = "x".to_owned();
    s1.push('Z');
    println!("{:p}", s2.as_ptr());
    println!("{:p} {} {}: {}",
    s1.as_ptr(), s1.capacity(), s1.len(), s1);

    /*
    iteration 0: the capacity is zero and no buffer is allocated; the address of such buffers is 1 which is an invalid memory address
    iteration 1: one character is appended, an 8-byte buffer is allocated
    iteration 2: from here to 8, no reallocation is required because the buffer is large enough
    iteration 9: buffer extends to 16 bytes. So, no deallocation and new allocation happens 
    Extention happens immediately after s1 because following the buffer of s1 is still free
    iteration 10: from here to 15, no reallocation is required
    When s2 is allocated, that buffer will go just after the current buffer for the s1 string
    When s1 is pushed again lastly, s1.push('Z'), we need more buffer but extenstion cannot be done
    because afer s1, we have s2, so we need to reallocate s1
    */

    // concatenation

    // static
    let sa = "Hello";
    // dynamic
    let da = "C alifornia".to_owned();
    let concat = format!("{}{}", sa, da);
    println!("concatenation of a static and dyamic string: {}", concat);
    
    // Reverse conversation from dynamic to static


    // Difference between vec<char> and string::from()
    let a120: vec<char> = ['a','b','c']; // 12 bytes
    let b121 = string::from("abc"); // 3 byte
    /*
    Vec<char>): Each character takes 4 bytes because Rust's char type is designed to hold any Unicode character. 
    Therefore, 'a', 'b', and 'c' will each use 4 bytes.  This is a collection of individual characters.
    (String): UTF-8 encoding is used in String, so ASCII characters (like 'a', 'b', and 'c') will take 1 byte each. 
    This makes String more memory-efficient for most cases.
    */


}

