use serde::{Serialize, Deserialize};
use serde_json::Error as SerdeError;
use std::fs::File;
use std::io::{self, Read, Write};

// Custom Error Type
#[derive(Debug)]
enum MyError {
    Io(io::Error),
    Serde(SerdeError),
}

impl From<io::Error> for MyError {
    fn from(err: io::Error) -> MyError {
        MyError::Io(err)
    }
}

impl From<SerdeError> for MyError {
    fn from(err: SerdeError) -> MyError {
        MyError::Serde(err)
    }
}

// Trait Declarations
trait ToJson {
    fn to_json(&self) -> String;
    fn save_to_file(&self, filename: &str) -> Result<(), MyError>;
}

trait FromJson: Sized {
    fn from_json(s: &str) -> Result<Self, SerdeError>;
    fn load_from_file(filename: &str) -> Result<Self, MyError>;
}

// Structs and Implementations
#[derive(Serialize, Deserialize, Debug)]
struct Address {
    street: String,
    city: String,
    country: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Person {
    name: String,
    age: u8,
    address: Address,
    phone: Option<String>,  // Optional field
}

impl ToJson for Person {
    fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }

    fn save_to_file(&self, filename: &str) -> Result<(), MyError> {
        let json_str = self.to_json();
        let mut file = File::create(filename)?;
        file.write_all(json_str.as_bytes())?;
        Ok(())
    }
}

impl FromJson for Person {
    fn from_json(s: &str) -> Result<Self, SerdeError> {
        serde_json::from_str(s)
    }

    fn load_from_file(filename: &str) -> Result<Self, MyError> {
        let mut file = File::open(filename)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        Person::from_json(&contents).map_err(MyError::from)
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Product {
    id: u32,
    name: String,
    price: f64,
    available: bool,
}

impl ToJson for Product {
    fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }

    fn save_to_file(&self, filename: &str) -> Result<(), MyError> {
        let json_str = self.to_json();
        let mut file = File::create(filename)?;
        file.write_all(json_str.as_bytes())?;
        Ok(())
    }
}

impl FromJson for Product {
    fn from_json(s: &str) -> Result<Self, SerdeError> {
        serde_json::from_str(s)
    }

    fn load_from_file(filename: &str) -> Result<Self, MyError> {
        let mut file = File::open(filename)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        Product::from_json(&contents).map_err(MyError::from)
    }
}

// Generic Function with Multiple bounds
fn process_and_load<T>(value: &T, filename: &str) -> Result<T, MyError>
where
/*
The where clause allows you to separate the trait bounds from the function signature, making it easier to 
read and manage, especially when there are multiple bounds or complex constraints. 
It also provides the flexibility to add more bounds or constraints if needed, without cluttering the function signature.
*/
    T: ToJson + FromJson, 
    /*
    This trait bound adds the capabilities of both traits to the capabilities of T. In
    this way, you can pick the traits you need for your generic type.
    */
{
    value.save_to_file(filename)?;
    T::load_from_file(filename)
}

// Main Function
fn main() -> Result<(), MyError> {
    let address = Address {
        street: "123 Main St".to_string(),
        city: "Somewhere".to_string(),
        country: "Neverland".to_string(),
    };

    let person = Person {
        name: "Alice".to_string(),
        age: 30,
        address,
        phone: Some("123-456-7890".to_string()), // Optional field
    };

    let product = Product {
        id: 101,
        name: "Laptop".to_string(),
        price: 999.99,
        available: true,
    };

    // Process and load Person
    let loaded_person: Person = process_and_load(&person, "person.json")?;
    // Process and load Product
    let loaded_product: Product = process_and_load(&product, "product.json")?;

    // Print loaded data
    println!("{:?}", loaded_person);
    println!("{:?}", loaded_product);

    Ok(())
}


/*

We compare the Rust & C++ in the followings:

#Rust:

"trait HasSquareRoot {
fn sqrt(self) -> Self;
}
impl HasSquareRoot for f32 {
fn sqrt(self) -> Self { self.sqrt() }
}
impl HasSquareRoot for f64 {
fn sqrt(self) -> Self { self.sqrt() }
}
trait HasAbsoluteValue {
fn abs(self) -> Self;
}
impl HasAbsoluteValue for f32 {
fn abs(self) -> Self { self.abs() }
}
impl HasAbsoluteValue for f64 {
fn abs(self) -> Self { self.abs() }
}
fn abs_quartic_root<Number>(x: Number) -> Number
where Number: HasSquareRoot + HasAbsoluteValue {
x.abs().sqrt().sqrt()
}
print!("{} {}",
abs_quartic_root(-100f64),
abs_quartic_root(-100f32));"

#C++:

#include <cmath>
#include <iostream>

// Base class template for HasSquareRoot
template<typename T>
class HasSquareRoot {
public:
    virtual T sqrt() const = 0;
};

// Specialization for float
class HasSquareRootFloat : public HasSquareRoot<float> {
public:
    float sqrt() const override {
        return std::sqrt(val);
    }
    explicit HasSquareRootFloat(float v) : val(v) {}
private:
    float val;
};

// Specialization for double
class HasSquareRootDouble : public HasSquareRoot<double> {
public:
    double sqrt() const override {
        return std::sqrt(val);
    }
    explicit HasSquareRootDouble(double v) : val(v) {}
private:
    double val;
};

// Base class template for HasAbsoluteValue
template<typename T>
class HasAbsoluteValue {
public:
    virtual T abs() const = 0;
};

// Specialization for float
class HasAbsoluteValueFloat : public HasAbsoluteValue<float> {
public:
    float abs() const override {
        return std::fabs(val);
    }
    explicit HasAbsoluteValueFloat(float v) : val(v) {}
private:
    float val;
};

// Specialization for double
class HasAbsoluteValueDouble : public HasAbsoluteValue<double> {
public:
    double abs() const override {
        return std::fabs(val);
    }
    explicit HasAbsoluteValueDouble(double v) : val(v) {}
private:
    double val;
};

// Generic function template with multiple bounds (using the two classes)
template<typename T>
T abs_quartic_root(const HasAbsoluteValue<T>& absVal, const HasSquareRoot<T>& sqrtVal) {
    T absValue = absVal.abs();
    T sqrtValue = sqrtVal.sqrt();
    return std::sqrt(sqrtValue);
}

int main() {
    HasAbsoluteValueDouble absValDouble(-100.0);
    HasSquareRootDouble sqrtValDouble(100.0);

    HasAbsoluteValueFloat absValFloat(-100.0f);
    HasSquareRootFloat sqrtValFloat(100.0f);

    std::cout << abs_quartic_root(absValDouble, sqrtValDouble) << " "
              << abs_quartic_root(absValFloat, sqrtValFloat) << std::endl;

    return 0;
}

C++ relies heavily on templates for generic programming, but templates do not enforce specific 
constraints on the types they operate on. This often leads to less informative error messages 
and difficulty in understanding the requirements of the template code.

C++ uses template specialization and SFINAE (Substitution Failure Is Not An Error) to enable 
complex generic programming, but this can be error-prone, hard to read, and difficult to debug.

C++ allows multiple inheritance, which can lead to issues like the diamond problem, where 
ambiguity arises in the inheritance hierarchy.

In C++, interface-like behavior is often achieved through abstract classes with pure virtual 
functions, which requires inheritance and can lead to tighter coupling between types.

*/