extern crate openssl;

use openssl::pkcs12::Pkcs12;
use std::fs::File;
use std::io::Read;

fn main() {
    let password = "6565abC@";
    let mut pfx_file = File::open("/home/pshko/sample.p12").unwrap();
    let mut pfx_data = Vec::new();
    pfx_file.read_to_end(&mut pfx_data).unwrap();

    let pkcs12 = Pkcs12::from_der(&pfx_data).unwrap();

    let parsed_pfx = pkcs12.parse2(password).unwrap();
    let cert = parsed_pfx.cert.expect("Certificate not found in PKCS#12 file");
    let pkey = parsed_pfx.pkey.expect("Private key not found in PKCS#12 file");

    let subject = cert.subject_name();
    let issuer = cert.issuer_name();
    let public_key = cert.public_key().unwrap();

    println!("Subject: {:?}", subject);
    println!("Issuer: {:?}", issuer);
    println!("Public Key: {:?}", public_key);
    println!("Private Key: {:?}", pkey);
}