/*
The impl statement can be used to add methods directly to a type and also it can be used to add
methods to a trait for a type. So a type can have two kinds of methods: those associated
through a trait, and those associated without specifying a trait. These last methods are
said to be inherent implementations, as they do not pass through a trait.
*/

// We cannnot declare or add an inherent implementation for a 
// primitive type, a type in standard library or in a third-part library. As follows:

// Suppose you want to add a method called `square` to the `i32` type
/*
impl i32 { // i32 is a primitive type
    fn square(self) -> i32 {
        self * self
    }
} // This approach is illegal because only the Rust language itself is allowed to define methods using an inherent implementation

fn main() {
    let x: i32 = 5;
    println!("{}", x.square()); // We want to call x.square(), but this won't compile
}

// Also, to avoid changing the behavior of external types in
surprising ways, Rust does not allow inherent implementations for external types.

The solution is this as we already discussed:

// Define a trait with the method you want to add
trait Square {
    fn square(self) -> Self;
}

// Implement the trait for the primitive type `i32`
impl Square for i32 {
    fn square(self) -> Self {
        self * self
    }
}

fn main() {
    let x: i32 = 5;
    // Now you can call the `square` method on an `i32` because it implements the `Square` trait
    println!("{}", x.square()); // This will print "25"
}

// Conclusion

This restriction prevents potential conflicts and ambiguities that could arise if multiple crates tried 
to add different methods with the same name to the same type. By using traits, you avoid these issues because 
traits allow multiple implementations to coexist, and method names are associated with the trait, not the type itself.

*/

// The Iterator Standard Trait :: Example: Iterating over HTTPS Responses

use reqwest::{self, Error};
use std::iter::Iterator;

// Define a struct that will hold the URLs to be processed.
struct UrlIterator {
    urls: Vec<String>,
    current: usize,
}

impl UrlIterator {
    fn new(urls: Vec<String>) -> Self {
        Self { urls, current: 0 }
    }
}

impl Iterator for UrlIterator {
    type Item = Result<String, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current < self.urls.len() {
            let url = &self.urls[self.current];
            self.current += 1;
            // Make the HTTP GET request and return the result.
            // `reqwest::blocking::get` returns a `Result` type which we match and handle properly.
            let response = reqwest::blocking::get(url)
                .and_then(|res| res.text());
            Some(response)
        } else {
            None
        }
    }
}

// Function to get the third HTTPS response body.
fn get_third_response<Iter>(mut iterator: Iter) -> Option<Iter::Item>
where
    Iter: Iterator,
{
    iterator.next(); // Skip the first URL
    iterator.next(); // Skip the second URL
    iterator.next()  // Return the result for the third URL
}

fn main() {
    // Example URLs to be fetched.
    let urls = vec![
        "https://example.com".to_string(),
        "https://httpbin.org/get".to_string(),
        "https://api.github.com".to_string(),
    ];

    let url_iter = UrlIterator::new(urls);

    match get_third_response(url_iter) {
        Some(Ok(body)) => println!("Third response body: {}", body),
        Some(Err(e)) => println!("Failed to fetch third URL: {}", e),
        None => println!("Less than three URLs provided."),
    }
}


