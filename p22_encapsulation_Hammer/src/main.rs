struct ToolBox { // The struct has four fields, each represented by a tuple
    hammer: (String, String),  // (Type, Details)
    wood: (String, String),    // (Type, Details)
    nail: (String, String),    // (Type, Details)
    saw: (String, String),     // (Type, Details)
}


impl ToolBox { // Both the name of the struct and the impl block must be the same. 
    // the impl block is a construct designed to encapsulate the methods associated to the type specified just after the impl keyword.

    fn new() -> Self {
    /*
    Self represents the type of the self; You can explicitly use the struct's name (Toolbox) instead of Self, 
    fn  new() -> Toolbox, but this is less common and doesn't provide the same flexibility. If so, then rest also changes as in B.
    A typical use of associated functions is for constructing new instances of the
    implemented type. Actually, in Rust you can also create any object without calling any
    method, though you can also encapsulate such creation in an associated function, like
    the new method. In Rust, constructors does not have to have the same name of the class. However, the
    convention is as follows: It has no arguments that returns an instance of the type Self.
    */
        Self { //  You typically need to use the Self {} block to construct and return an instance of the struct
            hammer: (String::new(), String::new()), // The property names must match the names defined in the struct
            wood: (String::new(), String::new()),
            // we can replace with the following variations to initialize the string field as follows:
            // hammer: ("".to_string(), "".to_string()) or wood: (string::from(""), string::from(""))
            nail: (String::new(), String::new()),
            saw: (String::new(), String::new()),
            // Each of the fields are initialized with an empty string. This what is happening here
        }

        /*
        B:
        Toolbox {
            hammer: (String::new(), String::new()),
            wood: (String::new(), String::new()),
            nail: (String::new(), String::new()),
            saw: (String::new(), String::new()),
        }
        The issue here is that if the name of the struct changes, we have to change this as well
        */
    }




    /*
    self, as in "fn set_hammer(&mut self," or other nethods, is a special argument that represents the current object, on which the method will be applied. 
    This method is declared for the ToolBox type, so here the type of self is implicitly Toolbox.
            
    A function declared in an impl block, but without a self argument, is actually not a
    method but an associated function. An associated function cannot access an instance of
    the type to which it is associated, because there is no such current object for it. Keeping
    it inside an impl block is just an encapsulation choice; it means that this function is
    considered to be strictly related to the currently implemented type, so it is available only in such scope.
    */

    // all our methods receive self by mutable reference.
    // A methods receives self by immutable reference corresponds to const method in C++
    fn set_hammer(&mut self, new_hammer: String, details: String) {
        /*
        The methods within an impl block are essentially functions where the instance (the self parameter) 
        is the first argument. This is a form of functional notation. 
        The instance is explicitly passed as the first parameter to the method.
        The self parameter allows methods to access and modify the instance of the struct they belong to.
        
        The name hammer in self.hammer refers to the field hammer in the Toolbox struct
        */
        self.hammer = (new_hammer, details);
    }

    fn set_wood(&mut self, new_wood: String, details: String) {
        self.wood = (new_wood, details);
    }

    fn set_nail(&mut self, new_nail: String, details: String) {
        self.nail = (new_nail, details);
    }

    fn set_saw(&mut self, new_saw: String, details: String) {
        self.saw = (new_saw, details);
    }

    fn display_tools(&self) {
        let header = format!("{:<10} {:<25} {:<50}", "Tool", "Type", "Details");
        let separator = format!("{:<10} {:<25} {:<50}", "----", "----", "-------");
        // format!("{:<10} {:<15} {:<20}", ...) ensures each column has a fixed width

        println!("{}", header);
        println!("{}", separator);
        println!("{:<10} {:<25} {:<50}", "Hammer", self.hammer.0, self.hammer.1);
        // We can access the elements of a tuple using dot notation followed by the index number.
        // self.hammer.0 refers to the first element of the tuple, which is a String.
        println!("{:<10} {:<25} {:<50}", "Wood", self.wood.0, self.wood.1);
        println!("{:<10} {:<25} {:<50}", "Nail", self.nail.0, self.nail.1);
        println!("{:<10} {:<25} {:<50}", "Saw", self.saw.0, self.saw.1);
    }

}

fn main() {

    #[warn(dead_code)]
    fn dot_notation_functional_notation(){

    /*
    Not every function can be invoked using the dot notation. For example, the
    String::new and File::open functions can only be invoked using the functional
    notation. However, any function that may be invoked using the dot notation may also be
    invoked using the functional notation.
    */

    println!("{} {}",
    "pshko".to_string(),
    std::string::ToString::to_string("pshko"));

    println!("{} {}",
    [1, 2, 3].len(),
    <[i32]>::len(&[1, 2, 3]));

    let mut v1 = vec![0u8; 0];
    let mut v2 = vec![0u8; 0];
    v1.push(7);
    Vec::push(&mut v2, 7);
    println!("{:?} {:?}", v1, v2);

    /*
    When the dot notation is transformed into the functional notation, the current object
    becomes an additional first argument. In this example, it would become f(a, b, c).
    Though, the current object must be decorated by the possibly required dereference
    symbol (&) or mutation keyword (mut), or both, like in f(&mut a, b, c). Such
    decorations are implicit, using the dot notation. a.f(b, c), => f(&mut a, b, c).
    With this way, there is a scoping issue. In an application, there may be several
    functions having the same name.
    */

    }
    
    let mut my_toolbox = ToolBox::new();
    // new here is the name of the constructor function: fn new() -> Self. Thus, it could be new_t1 then Toolbox::new_t1()
    my_toolbox.set_hammer("Heavy Duty Hammer".to_string(), "Used for large projects".to_string());
    my_toolbox.set_wood("Oak Wood".to_string(), "High quality hardwood".to_string());
    my_toolbox.set_nail("2-inch Nail".to_string(), "Ideal for general carpentry".to_string());
    // each string is converted by "to_string()", even if we used other methods in the class methods of constructors:
    // hammer: ("".to_string(), "".to_string()) or wood: (string::from(""), string::from(""))
    my_toolbox.set_saw("Hand Saw".to_string(), "For manual cutting of wood".to_string());

    my_toolbox.display_tools();

}

    
