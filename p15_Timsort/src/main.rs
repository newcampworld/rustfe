fn insertion_sort<T: Ord>(arr: &mut [T], left: usize, right: usize) {
    for i in left + 1..=right {
        let mut j = i;
        let tmp = arr[i];

        while j > left && arr[j - 1] > tmp {
            arr[j] = arr[j - 1];
            j -= 1;
        }

        arr[j] = tmp;
    }
}

fn merge<T: Ord + Copy>(arr: &mut [T], left: usize, mid: usize, right: usize) {
    let mut left_arr = arr[left..=mid].to_vec();
    let mut right_arr = arr[mid + 1..=right].to_vec();

    left_arr.push(T::max_value());
    right_arr.push(T::max_value());

    let (mut left_idx, mut right_idx, mut dest_idx) = (0, 0, left);

    while dest_idx <= right {
        if left_arr[left_idx] <= right_arr[right_idx] {
            arr[dest_idx] = left_arr[left_idx];
            left_idx += 1;
        } else {
            arr[dest_idx] = right_arr[right_idx];
            right_idx += 1;
        }
        dest_idx += 1;
    }
}

fn timsort<T: Ord + Copy>(arr: &mut [T]) {
    let mut min_run = 32;
    let n = arr.len();

    for start in (0..n).step_by(min_run) {
        let end = if start + min_run - 1 > n - 1 {
            n - 1
        } else {
            start + min_run - 1
        };

        insertion_sort(arr, start, end);
    }

    for size in (min_run..n).step_by(min_run) {
        for left in (0..n).step_by(size * 2) {
            let mid = if left + size - 1 > n - 1 {
                n - 1
            } else {
                left + size - 1
            };

            let right = if left + 2 * size - 1 > n - 1 {
                n - 1
            } else {
                left + 2 * size - 1
            };

            merge(arr, left, mid, right);
        }
    }
}

fn main() {
    let mut arr = vec![5, 2, 8, 1, 9, 3];
    timsort(&mut arr);
    println!("{:?}", arr); // Output: [1, 2, 3, 5, 8, 9]
}

/*

The Timsort algorithm is named after its inventor, Tim Peters. 
He created and implemented the algorithm in 2002 for use in the Python 
programming language's sorted() function and list.sort() method.
The name "Timsort" combines Tim's name with the word "sort" to 
create a name for the sorting algorithm he developed.
Tim Peters was a member of the Python core development team and 
the principal author of the "Sorting Mini-HOWTO" in the Python 
documentation. He designed Timsort as an adaptive, stable sorting 
algorithm that combines the benefits of Insertion Sort and Merge Sort 
to provide efficient sorting performance across a wide range of input data 
patterns.

*/