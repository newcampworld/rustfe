/*
+-----------------------+
|       Block Header    | (80 bytes total)
+-----------------------+
| - Version             | 4 bytes
| - Previous Block Hash | 32 bytes
| - Merkle Root         | 32 bytes
| - Timestamp           | 4 bytes
| - Difficulty Target   | 4 bytes
| - Nonce               | 4 bytes
+-----------------------+
|  Transaction Counter  | Variable (1–9 bytes)
+-----------------------+
|     Transactions      | (Variable size)
+-----------------------+
| - Transaction 1       | (Coinbase Transaction)
|   - Version           | 4 bytes
|   - Input Count       | Variable (1–9 bytes)
|   - Inputs            | Variable
|     - Previous Output Hash | 32 bytes (all zeros for coinbase)
|     - Output Index         | 4 bytes (0xFFFFFFFF for coinbase)
|     - Script Length        | Variable (1–9 bytes)
|     - ScriptSig            | Variable (includes the message "The Times 03/Jan/2009 Chancellor on brink of second bailout for banks")
|     - Sequence             | 4 bytes
|   - Output Count      | Variable (1–9 bytes)
|   - Outputs           | Variable
|     - Value                | 8 bytes (50 BTC, represented in satoshis)
|     - Script Length        | Variable (1–9 bytes)
|     - ScriptPubKey         | Variable (locking script specifying miner's address)
|   - Lock Time         | 4 bytes
+-----------------------+
*/

use sha2::{Sha256, Digest};

fn main() {
    // Genesis Block Header (80 bytes)
    let block_header_hex = concat!(
        // The concat! macro performs concatenation during compile time, 
        // meaning the result is directly embedded in the compiled binary. Only works with static string literals.
        "01000000", // Version
        "0000000000000000000000000000000000000000000000000000000000000000", // Previous Block Hash
        "4a5e1e4baab89f3a32518a881073928ec7b169a3edfb5a6b44c595a06bd495a5", // Merkle Root
        "496b544f", // Timestamp (1231006505, Jan 3, 2009)
        "1d00ffff", // Difficulty Target
        "29ab5f49"  // Nonce
    );

    /*
    All data, including the block header, transactions, and metadata like sender, receiver, and fees, 
    is serialized into binary format before being added to a block. This serialization ensures that 
    the data is compact, standardized, and ready for hashing. However, The data here was written 
    directly as a hexadecimal string. This is done for the sake of demonstration here. 
    Note:
        The substring 0x is not valid within a pure hex string. 
        Hex strings cannot contain extra characters like 0x or x
    */

    // Transaction Counter (1 byte, 1 transaction)
    let transaction_counter_hex = "01";

    // Coinbase Transaction (Variable size)
    let coinbase_transaction_hex = concat!(
        "01000000", // Transaction Version
        "01", // Input Count (1 input)
        "0000000000000000000000000000000000000000000000000000000000000000", // Previous Output Hash
        "ffffffff", // Output Index
        "29", // Script Length (41 bytes)
        "5468652054696d65732030332f4a616e2f32303039204368616e63656c6c6f72", // ScriptSig (Message Part 1)
        "206f6e206272696e6b206f66207365636f6e64206261696c6f757420666f7220", // ScriptSig (Message Part 2)
        /*
        ScriptSig typically includes the signature and the public key of the spender.
        The Genesis Block is a special case because there is no "previous output." 
        Therefore, the ScriptSig contains arbitrary data (commonly called the coinbase field).
        This data includes:
        The ASCII message: "The Times 03/Jan/2009 Chancellor on brink of second bailout for banks".
        */
        "62616e6b73", // ScriptSig (Message Part 3)
        "01", // Output Count (1 output)
        "00f2052a01000000", // Value (50 BTC, 5000000000 satoshis)
        "43", // Script Length (67 bytes)
        "4104678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61", // ScriptPubKey (Part 1)
        "deb649f6bccf4f5f7254e1ecf4a1d79bf67300b970a4a23fd69ec7e8d0a00000", // ScriptPubKey (Part 2)
        "000000000000000000000000", // ScriptPubKey (Part 3)
        "00000000" // Lock Time
    );

    // Full Genesis Block (Concatenate Header + Transaction Counter + Coinbase Transaction)
    // Instead of using concate again, we utilize format because it works with runtime or dynamic data.
    let genesis_block_hex = format!("{}{}{}", block_header_hex, transaction_counter_hex, coinbase_transaction_hex);
    
    /*
    hex::decode decodes a hexadecimal string into a binary Vec<u8>
    Each pair of hexadecimal digits (e.g., "AB") translates to one byte (0xAB) = 01000001 01000010
    Input here should be of multiple 2 otherwise error 'Err(OddLength)' is generated as in the following example:
        let b1 = concat!("0100000", "95ABCDEF");
        let byte_block = hex::decode(b1);
        println!("{:?}", byte_block);
    Because the whole string becomes 15 characters!
    */
    let genesis_block_bytes = hex::decode(genesis_block_hex).expect("Invalid hex string");

    /*
    Sha256::digest takes a slice of bytes (&[u8]) as input; hex::decode has already produced a vec<u8>.
    Instead of manually creating a Sha256 hasher, updating it with data, and finalizing the hash, 
    digest encapsulates this process into a single step. This is ideal for hashing a single, 
    self-contained piece of data (e.g., a file, serialized structure, or transaction). 
    You can pass the data as a byte slice (&[u8]) and directly get the hash result. 
    */
    let first_hash = Sha256::digest(&genesis_block_bytes);
    let second_hash = Sha256::digest(&first_hash);
    /*
    If your data is large or arrives in chunks (e.g., streaming a file), 
    digest won’t work because it expects the entire input at once.
        let mut hasher = Sha256::new();
        hasher.update(b"Part 1"); // 
        hasher.update(b"Part 2");
        let result = hasher.finalize();
    */

    println!("First  Hash: {:x}", first_hash);
    println!("Second Hash: {:x}", second_hash);
    // Convert the final hash to hexadecimal and reverse for Bitcoin's little-endian format
    let final_hash: String = second_hash.iter().rev().map(|b| format!("{:02x}", b)).collect();
    // format!("{:02x}", b) converts the byte (b) into a two-character lowercase

    // Print the Genesis Block hash
    println!("Genesis Block Hash: {}", final_hash);
}

/*
Genesis block's Previous Block Hash is filled with zeros, and it contains a single transaction, 
a coinbase transaction, which creates 50 BTC as a reward for mining. This transaction includes 
an arbitrary message in its ScriptSig: "The Times 03/Jan/2009 Chancellor on brink of second 
bailout for banks."

The Merkle Root is derived by hashing the  sole transaction (the coinbase transaction) using a Merkle Tree process. 
The block header is then hashed twice using SHA-256 to produce the block’s unique hash: 
000000000019d6689c085ae165831e936d9f261731ce8a147e1eec05e4a4d051. This hash is not stored in 
the block itself but is used as a reference in the next block’s Previous Block Hash field, 
forming the first link in the blockchain.

When nodes validate subsequent blocks, they compute the hash of the Genesis Block’s header and 
compare it with the Previous Block Hash in Block 1. This linking of blocks ensures immutability, 
as altering any part of the Genesis Block (or any block in the chain) would invalidate all subsequent blocks. 
*/

/*
For some reason, we are not able to get the following hash!
000000000019d6689c085ae165831e936d9f261731ce8a147e1eec05e4a4d051
*/