/*
The data structure names prefixed with Hash are unordered ones but one suffixed with Map are ordered
BinaryHeap is the only one uses pop and push among the following
*/
use std::collections::{LinkedList, BinaryHeap, BTreeSet, HashSet, BTreeMap, HashMap};

fn linked_list_example() {
    let mut list = LinkedList::new();
    list.push_back(10);
    list.push_back(20);
    list.push_back(30);

    println!("LinkedList contents:");
    for value in &list {
        println!("{}", value);
    }
}

// BinaryHeap
/* This is a max-heap by default and no duplicates is allowed.
If you want to iterate over the heap, you need to remove elements 
using pop() to retrieve them in priority order (largest to smallest for a max-heap).
The array inside the BinaryHeap might look unordered overall, but the root element 
(index 0) always holds the highest priority element.
If you need to preserve the heap while printing, you can clone the heap first:
let mut cloned_heap = heap.clone(); 
Use BinaryHeap when: You need a priority queue & fast access to the largest or smallest element. */
fn binary_heap_example() {
    let mut heap = BinaryHeap::new();
    heap.push(10);
    heap.push(20);
    heap.push(16);
    heap.push(9);
    heap.push(8);
    heap.push(6);
    heap.push(14);
    heap.push(12);

    println!("Heap (internal representation): {:?}", heap);
    /* After pushing, the heap reorders itself to maintain the max-heap property
    which is to guarante that root is the largest element. So, in each pop()
    the largest element is moved to peak */
    println!("Largest element: {:?}", heap.peek());
    println!("\nBinaryHeap contents (max-heap):");
    while let Some(value) = heap.pop() {
        println!("{}", value);
    }

    // For String, the root is the lexicographically largest string in a max-heap.
}

// Ordered set
// BTreeSet stores unique elements in sorted ascending order using a balanced binary search tree (B-Tree)
fn btree_set_example() {
    let mut set = BTreeSet::new();
    set.insert(String::from("apple"));
    set.insert(String::from("banana"));
    set.insert(String::from("orange"));
    set.insert(String::from("watermelon"));
    set.insert(String::from("pineapple"));
    set.insert(String::from("peach"));

    /* The BTreeSet will store them in sorted (ascending) lexicographical order.
    The set will look like this internally:
    ["apple", "banana", "orange", "peach", "pineapple", "watermelon"] */
    println!("BTreeSet contents: {:?}", set);

    // Remove an element by borrowing (&String).
    // `remove()` requires `&String`, so you need to provide it explicitly
    if set.remove(&String::from("apple")) {
        /* Here, "apple" is temporarily created as a String (using String::from())
        The & operator borrows this temporary string as a reference (&String),
        which is what the remove() method expects, and is better than copying and taking its ownership.
        The borrowed reference is used by remove() to compare it with the elements 
        already stored in the BTreeSet (internally, comparisons happen using the Ord trait).
        If remove() took ownership of the string, it would consume the temporary string, but consuming 
        it serves no purpose for a comparison operation. Borrowing avoids wasting memory and computation.
        All in all, it avoids Ownership Conflicts and Unnecessary Copying */
        println!("'apple' removed.");
    }

    // Check membership
    // `contains()` works with `&str` due to Borrow trait internally implemented
    if set.contains("banana") {
        println!("'banana' is in the set.");
    }

    /*
    When calling remove() or contains(), you’re just asking the collection to "look at" its elements 
    to perform an operation instead of moving or copying the value you’re working with.
    */

    // Iterate over the sorted elements
    for value in &set {
        println!("{}", value); //
    }

    /*
    If the BtreeSet were a set of number, we could use the following queries to retrieve all numbers in a range:
    set.insert(23);
    set.insert(29);
    set.insert(36);
    let range = set.range(20..=40);
    for value in range {
        println!("{}", value); // Outputs: 20, 30, 40
    }
    We should remeber that BTreeSet does not support inserting ranges directly (e.g., set.insert(10..=20)
    */
}

// Unordered set
/* HashSet is an unordered collection of unique values, implemented using a hash table 
for fast operations. Since elements are hashed, their order is not maintained, making 
HashSet ideal for scenarios where order doesn't matter but uniqueness does, such as 
membership checks or removing duplicates from a dataset */
fn hash_set_example() {
    let mut set = HashSet::new();
    set.insert("apple");
    set.insert("banana");
    set.insert("apple"); // Duplicate, ignored

    println!("\nHashSet contents (unordered set):");
    for value in &set {
        println!("{}", value);
    }
}

// Ordered dictionary (map)
// BTreeMap maintains elements in sorted order, 
// making it ideal for scenarios where both fast access and sorted data are required
fn btree_map_example() {
    let mut altitude_zones = BTreeMap::new();

    // Use integers as keys for demonstration
    altitude_zones.insert(0, "Low Altitude");
    altitude_zones.insert(5001, "Mid Altitude");
    altitude_zones.insert(20001, "High Altitude");
    altitude_zones.insert(40001, "Very High Altitude");
    altitude_zones.insert(60001, "Space Transition");

    // Query a range of keys
    for (altitude, description) in altitude_zones.range(5001..40002) {
        println!("{}: {}", altitude, description);
    }
}

// Unordered dictionary (map)
fn hash_map_example() {
    let mut map = HashMap::new();
    map.insert("key1", 10);
    map.insert("key2", 20);
    map.insert("key1", 15); // Overwrites previous value

    println!("\nHashMap contents (unordered dictionary):");
    for (key, value) in &map {
        println!("{} -> {}", key, value);
    }
}

fn main() {

    // linked_list_example();
    // binary_heap_example();
    // btree_set_example();
    // hash_set_example();
    btree_map_example();
    // hash_map_example();

}
