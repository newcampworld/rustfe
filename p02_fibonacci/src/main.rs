fn main() {
    let mut fibonacci = [1; 12];
    for i in 2..fibonacci.len() {
        fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
    }
    for i in 0..fibonacci.len(){
        println!("index {}, {}", i, fibonacci[i]);
    }
}
