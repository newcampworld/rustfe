use chrono::Local;

fn main() {
    // Get the current date and time
    let now = Local::now();
    
    // 12-hour format with AM/PM
    println!("\nCurrent date and time (12-hour format): {}\n", now.format("%I:%M:%S %p"));

    /*
    %I: This represents the hour in 12-hour format, with leading zeros
    %p: This stands for AM or PM depending on the time of day, and is used alongside the 12-hour format (%I).
    */

    // Milliseconds in 12-hour format
    println!("Milliseconds time: {}", now.format("%I:%M:%S.%3f %p"));
    // Microseconds in 12-hour format
    println!("Microseconds time: {}", now.format("%I:%M:%S.%6f %p"));
    // Nanoseconds in 12-hour format
    println!("Nanoseconds  time: {}", now.format("%I:%M:%S.%9f %p\n"));
}
