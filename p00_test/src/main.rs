use futures::future::join_all; 
use tokio::fs::File as AsyncFile;
use tokio::io::{AsyncWriteExt, AsyncReadExt};
use tokio::time::{self, Duration};
use std::io;

async fn run_async() -> io::Result<()> {
    let mut t1 = AsyncFile::create("task1").await?;
    t1.write_all("task1".as_bytes()).await?;

    let mut t2 = AsyncFile::create("task2").await?;
    t2.write_all("task2".as_bytes()).await?;

    let mut t3 = AsyncFile::create("task3").await?;
    t3.write_all("task3".as_bytes()).await?;

    let mut t4 = AsyncFile::create("task4").await?;
    t4.write_all("task4".as_bytes()).await?;
}

#[tokio::main]
async fn main() -> io::Result<()> {
    run_async().await?;
    Ok(())
}