use futures::future::join_all;
use tokio::fs::File as AsyncFile;
use tokio::io::AsyncWriteExt;
use std::io;
use std::future::Future;
use std::pin::Pin;

async fn run_async() -> io::Result<()> {
    let filenames = vec!["task1.txt", "task2.txt", "task3.txt", "task4.txt"];

    let mut tasks: Vec<Pin<Box<dyn Future<Output = io::Result<()>>>>> = Vec::new();

    for filename in &filenames {
        tasks.push(Box::pin(async move {
            let mut file = AsyncFile::create(filename).await?;
            file.write_all(b"Hello!").await?;
            println!("Finished writing: {}", filename);
            Ok::<(), io::Error>(())
        }));
    }

    join_all(tasks).await.into_iter().collect::<Result<Vec<_>, _>>()?;

    Ok(())
}

#[tokio::main]
async fn main() -> io::Result<()> {
    run_async().await?;
    println!("All tasks completed.");
    Ok(())
}

/* Collecting async tasks before execution is essential when we want to run multiple independent 
operations concurrently. If we do not collect them, we risk running them sequentially, which 
defeats the purpose of async programming in many cases. The reason for collecting tasks is that 
Tokio’s executor needs a way to poll multiple futures at once; without this, it can only drive 
one future at a time. By using methods like .map() to lazily produce futures or a Vec<T> to 
store them explicitly, we ensure that the executor has multiple tasks ready to be scheduled 
simultaneously. This is especially useful when dealing with file I/O, network requests, or 
any operation that involves waiting on external resources.

However, collecting futures comes with some caveats. Rust requires that all elements inside a Vec<T> 
must have the same type, but each async move { ... } block generates a unique, anonymous future type. 
This means that if we try to push raw async blocks into a vector, we will run into type mismatch errors. 
The most common way to fix this is by boxing the futures with Box::pin(), which allows the compiler 
to store different async blocks under a shared trait (dyn Future). Alternatively, we can use an 
iterator (.map()) instead of a Vec, which prevents type mismatch issues by directly passing the 
iterator to join_all(), avoiding unnecessary heap allocations.

Despite the need to collect futures in most cases, there are exceptions. When using tokio::spawn(), 
we do not need to collect tasks because each spawned task is immediately scheduled and managed 
by the runtime. This is useful when we want independent tasks that run in the background without 
requiring us to await them together. In structured concurrency scenarios, however, collecting 
tasks ensures that we maintain proper control over execution, error handling, and resource management. 
Therefore, whether or not we collect tasks depends on whether we need structured concurrency 
(join_all() with a Vec) or detached execution (tokio::spawn). */